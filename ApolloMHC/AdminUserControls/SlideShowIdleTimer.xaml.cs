﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;

namespace ApolloMHC.AdminUserControls
{
    /// <summary>
    /// Interaction logic for SlideShowIdleTimer.xaml
    /// </summary>
    public partial class SlideShowIdleTimer : UserControl, IDisposable
    {
        #region variables
        public event EventHandler EventCloseIdleTimer;
        public event EventHandler EventSuccessAlert;
        public event EventHandler EventWarningAlert;
        private Configuration config;
        private int idleTimerSeconds;
        private Logger logger = LogManager.GetCurrentClassLogger();
        #endregion
        public SlideShowIdleTimer()
        {
            InitializeComponent();
        }

        private void btnBack_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            Apps.KillOSK();
            EventCloseIdleTimer(this, null);
            logger.Info("Completed");
        }
        private void btnSave_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            int idleSeconds = 0;
            if (config == null)
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            }

            if (Int32.TryParse(txtTimer.Text.Trim(), out idleSeconds))
            {
                if (idleSeconds > 120)
                {
                    config.AppSettings.Settings["IdleTimer"].Value = idleSeconds.ToString();
                    config.Save(ConfigurationSaveMode.Full, true);
                    EventSuccessAlert("Details Saved Successfully !!!", null);
                    btnBack_TouchDown(sender, e);
                }
                else
                {
                    EventWarningAlert("Idle seconds must be greater than 120 seconds", null);
                    logger.Info("Idle seconds must be greater than 120 seconds");
                }
            }
            logger.Info("Completed");
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            if (!(Int32.TryParse(config.AppSettings.Settings["IdleTimer"].Value, out idleTimerSeconds)) && idleTimerSeconds <= 120)
            {
                idleTimerSeconds = 120;
            }
            txtTimer.Text = idleTimerSeconds.ToString();
            logger.Info("Completed");
        }
        private void txtTimer_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }

        private void txtTimer_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            txtTimer.Focus();
        }

        public void Dispose()
        {
            EventCloseIdleTimer = null;
            EventSuccessAlert = null;
            EventWarningAlert = null;
            config = null;
        }
    }
}
