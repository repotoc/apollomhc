﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloMHC.AdminUserControls
{
    /// <summary>
    /// Interaction logic for SetAutoSyncTime.xaml
    /// </summary>
    public partial class SetAutoSyncTime : UserControl, IDisposable
    {

        #region variables
        private Logger logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseSetAutoSync;
        public event EventHandler autoSyncRefresh;
        public event EventHandler EventSuccessAlert;
        public event EventHandler EventErrorAlert;
        public event EventHandler EventSyncNow;
        Configuration config;
        Timer currentTimer;
        #endregion

        public SetAutoSyncTime()
        {
            InitializeComponent();
            currentTimer = new Timer();
            currentTimer.Interval = 1000;
            currentTimer.Elapsed += delayTimer_Elapsed;
            currentTimer.Start();
        }

        void delayTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                currentClock.Time = DateTime.Now;
            }));
        }
        private void btnBack_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            currentTimer.Stop();
            currentTimer = null;
            EventCloseSetAutoSync(this, null);
            logger.Info("Completed");
        }

        private void btnSave_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            try
            {
                string time = Convert.ToString(syncTimePicker.SelectedTime);
                Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                config.AppSettings.Settings["AutoSyncTiming"].Value = time;
                config.AppSettings.Settings["AutoSyncEnabled"].Value = Convert.ToString(chkAutoSync.IsChecked, CultureInfo.CurrentCulture);
                config.Save();
                autoSyncRefresh(this, null);
                string Message = "Auto Sync Details Saved Successfully !!";
                EventSuccessAlert(Message, null);
                currentTimer.Stop();
                currentTimer = null;
                EventCloseSetAutoSync(this, null);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                EventErrorAlert(ex.Message, null);
            }
            logger.Info("Completed");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            bool autoSyncEnabled = false;
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            if (!string.IsNullOrEmpty(config.AppSettings.Settings["AutoSyncTiming"].Value))
            {
                DateTime autoSyncTime = Convert.ToDateTime(config.AppSettings.Settings["AutoSyncTiming"].Value);
                syncTimePicker.SelectedTime = autoSyncTime;
                chkAutoSync.IsChecked = Boolean.TryParse(config.AppSettings.Settings["AutoSyncEnabled"].Value, out autoSyncEnabled) ? autoSyncEnabled : false;
            }
            logger.Info("Completed");
        }

        private void btnSyncNow_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            try
            {
                EventSyncNow(this, null);
                if (currentTimer != null)
                {
                    currentTimer.Stop();
                    currentTimer = null;
                }
                EventCloseSetAutoSync(this, null);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                EventErrorAlert(ex.Message, null);
            }
            logger.Info("Completed");
        }

        public void Dispose()
        {
            if (currentTimer != null)
            {
                currentTimer.Stop();
                currentTimer = null;
            }
            EventCloseSetAutoSync = null;
        }
    }
}
