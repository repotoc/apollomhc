﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloMHC.AdminUserControls
{
    /// <summary>
    /// Interaction logic for AdminSettings.xaml
    /// </summary>
    public partial class AdminSettings : UserControl, IDisposable
    {
        #region variables
        public event EventHandler EventLogoutSettings;
        public event EventHandler EventEmailConfiguration;
        public event EventHandler EventSlideShowIdleTimer;
        public event EventHandler EventOpenSetAutoSync;
        private Logger logger = LogManager.GetCurrentClassLogger(); 
        #endregion
        public AdminSettings()
        {
            InitializeComponent();
        }
        private void btnLogout_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventLogoutSettings(this, null);
            logger.Info("Completed");
        }
        private void BtnApplicationIdle_OnTouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventSlideShowIdleTimer(this, null);
            logger.Info("Completed");
        }
        private void btnSetAutoSync_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventOpenSetAutoSync(this, null);
            logger.Info("Completed");
        }

        private void btnShowDesktop_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            System.Diagnostics.Process.Start("explorer");
            logger.Info("Completed");
        }

        private void btnRestart_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            if (MessageBox.Show("Are you sure want to restart the system.", "ApolloMHC", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/r /f /t 0");
            }
            logger.Info("Completed");
        }

        private void btnShutdown_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            if (MessageBox.Show("Are you sure want to shutdown the system.", "ApolloMHC", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");
            }
            logger.Info("Completed");
        }

        private void btnEmailConfiguration_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventEmailConfiguration(this, null);
            logger.Info("Completed");
        }

        public void Dispose()
        {
            EventLogoutSettings = null;
            EventEmailConfiguration = null;
            EventSlideShowIdleTimer = null;
            EventOpenSetAutoSync = null;
        }
    }
}
