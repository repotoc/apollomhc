﻿using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloMHC.AdminUserControls
{
    /// <summary>
    /// Interaction logic for AdminLogin.xaml
    /// </summary>
    public partial class AdminLogin : UserControl, IDisposable
    {
        #region variables
        public event EventHandler EventCloseAdminSetting;
        public event EventHandler EventLoginAdmin;
        public event EventHandler EventWarningAlert;
        private Logger logger = LogManager.GetCurrentClassLogger(); 
        #endregion

        public AdminLogin()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtName.Focus();
        }

        private void txtPassword_TouchEnter(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtName_TouchEnter(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }
        private void btnLogin_TouchDown(object sender, TouchEventArgs e)
        {
            ValidateLogin();
        }

        private void ValidateLogin()
        {
            logger.Info("Initiated");
            CloseTabTip();
            if (!string.IsNullOrEmpty(txtName.Text) && !string.IsNullOrEmpty(txtPassword.Password))
            {
                if (Apps.PingCheck())
                {
                    if (Apps.ValidateAdmin(txtName.Text, txtPassword.Password))
                    {
                        SaveCredentials(txtName.Text.Trim(), txtPassword.Password.Trim());
                        EventLoginAdmin(this, null);
                    }
                    else
                    {
                        EventWarningAlert("Invalid username and password.", null);
                        logger.Info("Invalid username and password");
                        ClearFields();
                    }
                }
                else if (txtName.Text.Trim().Equals(Utilities.UserName) && txtPassword.Password.Trim().Equals(Utilities.Password))
                {
                    EventLoginAdmin(this, null);
                }
                else
                {
                    ClearFields();
                    EventWarningAlert("Invalid username and password.", null);
                    logger.Info("Invalid username and password");
                }
            }
            else
            {
                ClearFields();
                EventWarningAlert("Username and Password should not be empty.", null);
                logger.Info("Username and password should not be empty");
            }
            logger.Info("Completed");
        }

        private void ClearFields()
        {
            txtName.Text = string.Empty;
            txtPassword.Password = string.Empty;
            txtName.Focus();
        }

        private void SaveCredentials(string userName, string password)
        {
            try
            {
                logger.Info("Initiated");
                Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                RegistryKey regKey = Registry.CurrentUser.OpenSubKey("Software\\TouchOnCloud\\CMC");
                if (regKey != null)
                {
                    config.AppSettings.Settings["UserName"].Value = Convert.ToString(regKey.GetValue("UserName"));
                    config.AppSettings.Settings["Password"].Value = Convert.ToString(regKey.GetValue("Password"));
                }
                else
                {
                    logger.Info("CMC UserName and Passwor are empty, so Slideshow and Offer images could'nt be downloaded");
                }
                config.AppSettings.Settings["Locationusername"].Value = userName;
                config.AppSettings.Settings["Locationpassword"].Value = password;
                config.Save(ConfigurationSaveMode.Full, true);
                logger.Info("Completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }

        private void btnCanelLogin_TouchDown(object sender, TouchEventArgs e)
        {
            CloseTabTip();
            EventCloseAdminSetting(this, null);
        }

        private void txtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                ValidateLogin();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                ValidateLogin();
            }
        }

        string processName;
        Process proc;
        private void OpenVirtualKeyboard()
        {
            try
            {
                CloseOSK();
                proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
                processName = proc.ProcessName;
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }
        private void CloseOSK()
        {
            Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
            foreach (Process proc in process)
            {
                proc.CloseMainWindow();    //kill the process
            }
        }
        private void CloseTabTip()
        {
            try
            {
                Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
                foreach (Process proc in process)
                {
                    proc.Kill();    //kill the process before closing usercontrol
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }

        private void GridMainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            CloseOSK();
        }

        private void txtName_TouchDown(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();

        }

        private void txtPassword_TouchDown(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();

        }

        public void Dispose()
        {
            EventCloseAdminSetting = null;
            EventLoginAdmin = null;
            EventWarningAlert = null;
            proc = null;
        }
    }
}
