﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloMHC.AdminUserControls
{
    /// <summary>
    /// Interaction logic for EmailConfigurationUC.xaml
    /// </summary>
    public partial class EmailConfigurationUC : UserControl, IDisposable
    {

        #region variables
        Configuration config;
        public event EventHandler EventCloseEmailSetting;
        public event EventHandler EventSuccessAlert;
        public event EventHandler EventWarningAlert;
        public event EventHandler EventErrorAlert;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        #endregion

        public EmailConfigurationUC()
        {
            InitializeComponent();
        }

        private void btnBack_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Apps.KillOSK();
            EventCloseEmailSetting(this, null);
            Logger.Info("Completed");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            txtUserEmail1.Text = config.AppSettings.Settings["EmailIDUser1"].Value;
            txtUserEmail2.Text = config.AppSettings.Settings["EmailIDUser2"].Value;
            txtEmail.Text = config.AppSettings.Settings["EmailID"].Value;
            txtPassword.Password = config.AppSettings.Settings["EmailPassword"].Value;
            txtSMTP.Text = config.AppSettings.Settings["SMTP"].Value;
            txtPortNumber.Text = config.AppSettings.Settings["Port"].Value;
            string sslValue=config.AppSettings.Settings["EnableSSL"].Value;
            if (!string.IsNullOrEmpty(sslValue))
            {
                if (Convert.ToBoolean(sslValue))
                {
                    chkSSL.IsChecked = true;
                }
                else
                {
                    chkSSL.IsChecked = false;
                }
            }
            Logger.Info("Completed");
        }

        private void btnSaveEmailDetails_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            string Message = string.Empty;
            if (!string.IsNullOrEmpty(txtEmail.Text) && !string.IsNullOrEmpty(txtPassword.Password) &&
                !string.IsNullOrEmpty(txtSMTP.Text) && !string.IsNullOrEmpty(txtPortNumber.Text))
            {
                config.AppSettings.Settings["EmailID"].Value = txtEmail.Text;
                config.AppSettings.Settings["EmailPassword"].Value = txtPassword.Password;
                config.AppSettings.Settings["SMTP"].Value = txtSMTP.Text;               //"smtp.gmail.com";
                config.AppSettings.Settings["Port"].Value = txtPortNumber.Text;         //587;
                if (chkSSL.IsChecked == true)
                {
                    config.AppSettings.Settings["EnableSSL"].Value = "true";
                }
                else
                {
                    config.AppSettings.Settings["EnableSSL"].Value = "false";
                }                
                config.Save();
                Message = "Details Saved Successfully !!!";
                EventSuccessAlert(Message, null);
                btnBack_TouchDown(sender, e);
            }
            else
            {
                Message = "Warning !!! Please enter valid details";
                Logger.Info("Warning !!! Please enter valid details");
                EventWarningAlert(Message, null);
            }
            Logger.Info("Completed");
        }

        private void textbox_TouchEnter(object sender, TouchEventArgs e)
        {
            Apps.KillOSK();
        }

        private void txtEmail_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        private void txtPassword_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        private void txtPortNumber_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        private void txtSMTP_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        private void txtUserEmail1_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        private void txtUserEmail2_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        private void txtSaveEmailDetails_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (ValidateEmail())
            {
                config.AppSettings.Settings["EmailIDUser1"].Value = txtUserEmail1.Text;
                config.AppSettings.Settings["EmailIDUser2"].Value = txtUserEmail2.Text;
                string Message = "Receive Mail Details Saved Successfully";
                EventSuccessAlert(Message, null);
            }
            Logger.Info("Completed");
        }

        private bool ValidateEmail()
        {
            Logger.Info("Initiated");
            string EmailID1 = txtUserEmail1.Text;
            string EmailID2 = txtUserEmail2.Text;
            string msg = string.Empty;
            int count = 0;
            if (!string.IsNullOrEmpty(EmailID1))
            {
                bool isEmail = Regex.IsMatch(EmailID1, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (!isEmail)
                {
                    msg = "Email ID 1 is not a valid Email ID";
                    count++;
                    EventErrorAlert(msg, null);
                }
            }
            if (!string.IsNullOrEmpty(EmailID2))
            {
                bool isEmail = Regex.IsMatch(EmailID2, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (!isEmail)
                {
                    msg = "Email ID 2 is not a valid Email ID";
                    count++;
                    EventErrorAlert(msg, null);
                }
            }
            bool result = (count == 0) ? true : false;
            Logger.Info("Completed");
            return result;
        }

        public void Dispose()
        {
            config = null;
            EventCloseEmailSetting = null;
            EventSuccessAlert = null;
            EventWarningAlert = null;
            EventErrorAlert = null;
        }
    }
}
