﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApolloMHC.Enums
{
    public enum Panels
    {
        AdminLogin,
        AdminSettings,
        EmailConfigurationUC,
        SetAutoSyncTime,
        SlideShowIdleTimer,
        ErrorAlertUC,
        SuccessAlertUC,
        UnderDevelopmentUC,
        WarningAlertUC,
        AutoSyncUC,
        BroucherUC,
        BrowserUc,
        CategoryUC,
        ContactUC,
        DepartmentUC,
        DocumentsUC,
        EmergencyUC,
        HomeMenuUC,
        ImageView,
        InternetDown,
        MHCFeedbackUC,
        SlideShowUC,
        TeamUC,
        VideoView
    }
}
