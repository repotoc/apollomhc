﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloMHC.UserControls.AlertMessageControls
{
    /// <summary>
    /// Interaction logic for WarningAlertUC.xaml
    /// </summary>
    public partial class WarningAlertUC : UserControl, IDisposable
    {
        Timer delayTimer;
        public WarningAlertUC()
        {
            InitializeComponent();
            delayTimer = new Timer();
            delayTimer.Interval = 2000;
            delayTimer.Elapsed += delayTimer_Elapsed;
            delayTimer.Start();
        }
        public event EventHandler EventCloseWarningAlert;
        void delayTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                Storyboard CloseAlert_SB = TryFindResource("CloseAlert_SB") as Storyboard;  // gets the CloseAlert_SB storyboard from resources
                CloseAlert_SB.Completed += new EventHandler(CloseAlert_SB_Completed);   //event raises when CloseAlert_SB completed
                CloseAlert_SB.Begin();    //starts CloseReviews_SB storyboard

            }));
        }
        void CloseAlert_SB_Completed(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                if (delayTimer != null)
                {
                    delayTimer.Stop();
                    delayTimer = null;
                }
                EventCloseWarningAlert(this, null);
            });
        }

        public void Dispose()
        {
            if (delayTimer != null)
            {
                delayTimer.Stop();
                delayTimer = null;
            }
            EventCloseWarningAlert = null;
        }
    }
}
