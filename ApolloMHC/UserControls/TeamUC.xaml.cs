﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for TeamUC.xaml
    /// </summary>
    public partial class TeamUC : UserControl, IDisposable
    {
        public TeamUC()
        {
            InitializeComponent();
        }
        #region variables
        public event EventHandler EventCloseTeam;
        public Department department { get; set; }
        private Logger logger = LogManager.GetCurrentClassLogger();        
        #endregion
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            if (department != null)
            {
                lstTeam.ItemsSource = department.LstDoctors.ToList().Where(c => c.IsActive).OrderBy(c => c.Order);
                btnCloseTeam.Content = string.Empty;
                btnCloseTeam.Content = department.Name.ToUpper();
            }
            else
            {
                EventCloseTeam(this, null);
            }
            logger.Info("Completed");
        }

        private void svDrTeam_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnCloseTeam_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventCloseTeam(this, null);
            logger.Info("Completed");
        }

        public void Dispose()
        {
            EventCloseTeam = null;
            department = null;
            lstTeam.ItemsSource = null;
        }
    }
}
