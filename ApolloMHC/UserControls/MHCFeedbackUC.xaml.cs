﻿using ApolloMHC.FeedbackQuestLogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloMHC.UserControls.AlertMessageControls;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for MHCFeedbackUC.xaml
    /// </summary>
    public partial class MHCFeedbackUC : UserControl, IDisposable
    {
        public MHCFeedbackUC()
        {
            InitializeComponent();
        }
        #region variables
        Process proc;
        private string processName;
        public event EventHandler EventCloseFeedbackMHC;
        public event EventHandler EventErrorAlert;
        public event EventHandler EventWarningAlert;
        List<FeedbackAns> lstAnswers;
        Configuration config;
        private Logger logger = LogManager.GetCurrentClassLogger();        
        #endregion

        private void btnBack_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventCloseFeedbackMHC(this, null);
            logger.Info("Completed");
        }

        private void ScrollViewer_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        private void OpenVirtualKeyboard()
        {
            try
            {
                CloseOSK();
                proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe");
                processName = proc.ProcessName;
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }
        private void CloseOSK()
        {
            try
            {
                Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
                foreach (Process proc in process)
                {
                    proc.CloseMainWindow();    //Close the process before closing usercontrol
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }

        private void txtYourName_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtUHIDNo_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtAddress_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtCity_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtState_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtBirthday_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtAnniversary_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtPincode_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtMobileNumber_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }
        private void txtEmail_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtDateOfVisit_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtFirstTimer_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtRepeat_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtRecognizeTeamMembr_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtSuggestions_TouchLeave(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void btnSubmitFeedback_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                logger.Info("Initiated");

                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                string email = config.AppSettings.Settings["EmailID"].Value;
                string EmailIDUser1 = config.AppSettings.Settings["EmailIDUser1"].Value;
                string EmailIDUser2 = config.AppSettings.Settings["EmailIDUser2"].Value;
                string password = config.AppSettings.Settings["EmailPassword"].Value;
                string smtp = config.AppSettings.Settings["SMTP"].Value;
                string port = config.AppSettings.Settings["Port"].Value;

                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(EmailIDUser1))
                {

                    if (Apps.PingCheck())
                    {
                        StringBuilder sb = new StringBuilder();
                        if (lstAnswers.Where(c => c.Rating > 0).ToList().Count > 1)
                        {
                            lstAnswers.Where(c => c.Rating > 0).ToList().ForEach(c =>
                            {
                                ApolloDB.Models.FeedBackQuestions question = Apps.myDbContext.FEEDBACKQUESTIONS.FirstOrDefault(d => d.ID.Equals(c.QuestionNumber));
                                if (question != null)
                                {
                                    ApolloDB.Models.FeedBackSubmits submit = new ApolloDB.Models.FeedBackSubmits();
                                    submit.ADDRESS = txtAddress.Text;
                                    if (!string.IsNullOrEmpty(datePckrAnniversary.Text))
                                    {
                                        submit.ANNIVERSARY = Convert.ToDateTime(datePckrAnniversary.Text, System.Globalization.CultureInfo.InvariantCulture);
                                    }
                                    if (!string.IsNullOrEmpty(datePckrBirthday.Text))
                                    {
                                        submit.BIRTHDAY = Convert.ToDateTime(datePckrBirthday.Text, System.Globalization.CultureInfo.InvariantCulture);
                                    }
                                    submit.CITY = txtCity.Text;
                                    submit.FIRSTTIME = txtFirstTimer.Text + txtSuggestions.Text;
                                    submit.GENDER = Convert.ToBoolean(rbFemale.IsChecked) ? "F" : "M";
                                    submit.ID = Apps.myDbContext.FEEDBACKSUBMITS.ToList().Count > 0 ? Apps.myDbContext.FEEDBACKSUBMITS.Max(d => d.ID) + 1 : 1;
                                    submit.LASTUPDATEDTIME = DateTime.Now;
                                    submit.MOBILENUMBER = txtMobileNumber.Text;
                                    submit.NAME = txtName.Text;
                                    submit.RATING = c.Rating;
                                    submit.PINCODE = txtPincode.Text;
                                    submit.REPEAT = txtRepeat.Text + txtRecognizeTeamMembr.Text;
                                    submit.STATE = txtState.Text;
                                    submit.UHIDNUMBER = txtUHIDNo.Text;
                                    submit.VISITDATE = DateTime.Now;
                                    submit.FEEDBACKQUEST = question;
                                    question.LSTFEEDBACKS.Add(submit);
                                    Apps.myDbContext.FEEDBACKSUBMITS.Add(submit);
                                    Apps.myDbContext.SaveChanges();
                                    sb.AppendFormat("<tr><td style='padding: 5px;border-left: 1px solid #f4f4f4;border-bottom: 1px solid #f4f4f4;'>{0}</td><td style='padding: 5px;border-left: 1px solid #f4f4f4;border-bottom: 1px solid #f4f4f4;'>{1}</td></tr>", question.QUESTION, c.Rating);
                                }
                            });
                            string mailString = sb.ToString();
                            string userName = txtName.Text;

                            if (!string.IsNullOrEmpty(EmailIDUser1))
                            {
                                string Message = string.Empty;
                                try
                                {
                                    MailMessage mail = new MailMessage(email, EmailIDUser1);
                                    if (!string.IsNullOrEmpty(EmailIDUser2))
                                    {
                                        mail.To.Add(EmailIDUser2);
                                    }
                                    SmtpClient sc = new SmtpClient(smtp, Convert.ToInt32(port));

                                    mail.Subject = "ApolloMHC User Feedback Mail";
                                    mail.IsBodyHtml = true;
                                    mail.Body = GenerateBody(userName, mailString);
                                    sc.Credentials = new System.Net.NetworkCredential(email, password);
                                    sc.EnableSsl = true;
                                    sc.Send(mail);

                                    string Msg = "Thanks for submitting Feedback !!!";
                                    SuccessAlertUC sAlert = new SuccessAlertUC();
                                    sAlert.tblSuccessMessage.Text = Msg;
                                    GridAlertPanel.Children.Add(sAlert);
                                    sAlert.EventCloseSuccessAlert += sAlert_EventCloseSuccessAlert;

                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex, ex.Message);
                                    EventErrorAlert(ex.Message, null);
                                }
                            }

                            //Task.Factory.StartNew(() =>
                            //{
                            //    SendMail(mailString, userName);
                            //});
                        }
                        else
                        {
                            string Msg = "Please answer atleast one !!!";
                            EventWarningAlert(Msg, null);
                        }
                    }
                }
                else { 
                    EventWarningAlert("EMail Configuration required, Please contact admin.", null);
                }

                logger.Info("Completed");

            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                EventErrorAlert(ex.Message, null);
            }
        }

        void sAlert_EventCloseSuccessAlert(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            SuccessAlertUC sAlert = (SuccessAlertUC)sender;
            GridAlertPanel.Children.Remove(sAlert);
            EventCloseFeedbackMHC(this, null);
            logger.Info("Initiated");
        }

        private void SendMail(string mailString, string userName)
        {
            logger.Info("Initiated");
            
            logger.Info("Completed");
        }

        private string GenerateBody(string userName, string mailString)
        {
            logger.Info("Initiated");
            string body = string.Empty;
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\EmailTemplate\\EmailTemplate.html";
            if (File.Exists(path))
            {
                string mailbody = System.IO.File.ReadAllText(path);
                using (StreamReader reader = new StreamReader(path))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{CustormerName}", userName);
                body = body.Replace("{FeedbackData}", mailString);
            }
            logger.Info("Completed");
            return body;
        }   
        
        private void feedbackRadioButton_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            RadioButton rb = (RadioButton)sender;
            if (rb != null)
            {
                int questionNbr = Convert.ToInt32(rb.GroupName.Substring(1));
                int rating = Convert.ToInt32(rb.Name.Substring(rb.Name.Length - 1));

                if (lstAnswers.Exists(c => c.QuestionNumber.Equals(questionNbr)))
                {
                    lstAnswers.FirstOrDefault(c => c.QuestionNumber.Equals(questionNbr)).Rating = rating;
                }
            }
            logger.Info("Completed");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");

            Apps.InsertPageHit("FeedbackUC");
            txtDateOfVisit.Text = DateTime.Now.ToShortDateString();
            lstAnswers = new List<FeedbackAns>();
            for (int i = 0; i < 27; i++)
            {
                lstAnswers.Add(new FeedbackAns(i + 1));
            }
            logger.Info("Completed");
        }

        public void Dispose()
        {
            proc = null;
            processName = null;
            EventCloseFeedbackMHC = null;
            EventErrorAlert = null;
            EventWarningAlert = null;
            lstAnswers = null;
            config = null;
        }
    }
}
