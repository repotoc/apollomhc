﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for EmergencyUC.xaml
    /// </summary>
    public partial class EmergencyUC : UserControl, IDisposable
    {
        public EmergencyUC()
        {
            InitializeComponent();
        }
        public event EventHandler EventCloseEmergencyUC;
        private Logger logger = LogManager.GetCurrentClassLogger();
        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            Storyboard ClosePage_SB = TryFindResource("ClosePage_SB") as Storyboard;
            ClosePage_SB.Completed += new EventHandler(ClosePage_SB_Completed);
            ClosePage_SB.Begin();
            logger.Info("Completed");
        }

        void ClosePage_SB_Completed(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            EventCloseEmergencyUC(this, null);
            logger.Info("Completed");
            
        }

        public void Dispose()
        {
            EventCloseEmergencyUC = null;
        }
    }
}
