﻿using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// 
    /// Interaction logic for BroucherUC.xaml
    /// </summary>
    public partial class BroucherUC : UserControl, IDisposable
    {
        public BroucherUC()
        {
            InitializeComponent();
        }
        #region variables
        public event EventHandler EventCloseBroucher;
        public int imageStartIndex = 0;
        private bool isNextActivated = false;
        private bool isPreviousActivated = false;
        private Logger logger = LogManager.GetCurrentClassLogger();
        public List<DocumentDetails> lstCategoryData; 
        #endregion

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventCloseBroucher(this, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            imageStartIndex = 0;
            lstCategoryData.Add(new DocumentDetails() { FileLocation = "../Icons/TheEnd.jpg", ImageExtension = "Jpg", WebFileLocation = "../Icons/TheEnd.jpg" });
            viewportRight.DataContext = lstCategoryData[imageStartIndex].FileLocation;
            logger.Info("Completed");
        }

        private void btnNext_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            if (lstCategoryData.Count > 1)
            {
                LoadNextButtonData();
            }
            logger.Info("Completed");
        }

        private void btnPrevious_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            if (lstCategoryData.Count > 1)
            {
                LoadPreviousButtonData();
            }
            logger.Info("Completed");
        }

        private void LoadNextButtonData()
        {
            logger.Info("Initiated");
            if (isPreviousActivated)
            {
                isPreviousActivated = false;
                imageStartIndex += 1;
            }
            if (imageStartIndex < lstCategoryData.Count - 1)
            {
                isNextActivated = true;
                if (ContinueToNext(imageStartIndex))
                {
                    viewportRightFlipUp.DataContext = lstCategoryData[imageStartIndex].FileLocation;
                    if (ContinueToNext(++imageStartIndex))
                    {
                        viewportLeftFlipDown.DataContext = lstCategoryData[imageStartIndex].FileLocation;
                        if (ContinueToNext(++imageStartIndex))
                        {
                            viewportRight.DataContext = lstCategoryData[imageStartIndex].FileLocation;
                        }
                        else
                        {
                            viewportRight.DataContext = "../Icons/watermark.jpg";
                        }
                    }
                    else
                    {
                        LoadBrocherEndPage();
                    }
                }
                else
                {
                    LoadBrocherEndPage();
                }
                Storyboard nxtSB = this.TryFindResource("SB_Next") as Storyboard;
                nxtSB.Completed -= new EventHandler(nxtSB_Completed);
                nxtSB.Completed += new EventHandler(nxtSB_Completed);
                nxtSB.Begin();
            }
            else
            {
                viewportLeftFlipUp.DataContext = null;
                viewportRightFlipUp.DataContext = null;
                viewportRightFlipDown.DataContext = null;
                //viewportLeftFlipDown.DataContext = null;
            }
            logger.Info("Completed");
        }

        private void LoadPreviousButtonData()
        {
            if (isNextActivated)
            {
                isNextActivated = false;
                imageStartIndex -= 1;
            }
            if (imageStartIndex > 0)
            {
                isPreviousActivated = true;
                if (ContinueToPrevious(imageStartIndex))
                {
                    viewportLeftFlipUp.DataContext = lstCategoryData[imageStartIndex].FileLocation;
                    if (ContinueToPrevious(--imageStartIndex))
                    {
                        viewportRightFlipDown.DataContext = lstCategoryData[imageStartIndex].FileLocation;
                        if (ContinueToPrevious(--imageStartIndex))
                        {
                            viewportLeft.DataContext = lstCategoryData[imageStartIndex].FileLocation;
                        }
                        else
                        {
                            viewportLeft.DataContext = null;
                        }
                    }
                    else
                    {
                        LoadBrocherStartPage();
                    }
                }
                else
                {
                    LoadBrocherStartPage();
                }
                if (imageStartIndex == 0)
                {
                    viewportLeft.Visibility = Visibility.Collapsed;
                }
                Storyboard prevSB = this.TryFindResource("SB_Previous") as Storyboard;
                prevSB.Completed -= new EventHandler(prevSB_Completed);
                prevSB.Completed += new EventHandler(prevSB_Completed);
                prevSB.Begin();
            }
            else
            {
                viewportLeftFlipUp.DataContext = null;
                viewportLeftFlipDown.DataContext = null;
                viewportRightFlipUp.DataContext = null;
                viewportRightFlipDown.DataContext = null;
                viewportLeft.DataContext = null;
            }
        }

        private void LoadBrocherStartPage()
        {
            if (imageStartIndex > -1)
            {
                viewportRight.DataContext = lstCategoryData[imageStartIndex].FileLocation;
                viewportLeft.DataContext = null;
            }
        }

        void nxtSB_Completed(object sender, EventArgs e)
        {
            if (ContinueToNext(imageStartIndex))
            {
                viewportLeft.DataContext = lstCategoryData[imageStartIndex].FileLocation;
            }
        }

        private void prevSB_Completed(object sender, EventArgs e)
        {
            if (imageStartIndex >= 0)
            {
                viewportRight.DataContext = lstCategoryData[imageStartIndex + 1].FileLocation;
            }

            if (imageStartIndex <= 0)
            {
                viewportRight.DataContext = lstCategoryData[0].FileLocation;
                viewportLeft.DataContext = null;
            }
        }

        private bool ContinueToNext(int imageIndex)
        {
            if (imageIndex < 0)
            {
                imageIndex = 0;
                imageStartIndex = 0;
            }
            return (imageIndex <= lstCategoryData.Count - 1) ? true : false;
        }

        private bool ContinueToPrevious(int imageIndex)
        {
            if (imageIndex == lstCategoryData.Count)
            {
                imageStartIndex = lstCategoryData.Count - 1;
                imageIndex = imageStartIndex;
            }
            return (imageIndex < lstCategoryData.Count && imageIndex >= 0) ? true : false;
        }

        private void LoadBrocherEndPage()
        {
            viewportRightFlipUp.DataContext = lstCategoryData[imageStartIndex - 2].FileLocation;
            viewportLeftFlipDown.DataContext = lstCategoryData[imageStartIndex - 1].FileLocation;
            viewportLeft.DataContext = lstCategoryData[imageStartIndex - 1].FileLocation;
            viewportRight.DataContext = null;
        }

        public void Dispose()
        {
            EventCloseBroucher = null;
            lstCategoryData = null;
        }
    }
}
