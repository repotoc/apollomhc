﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using System.Windows.Media.Animation;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for CategoryUC.xaml
    /// </summary>
    public partial class CategoryUC : UserControl, IDisposable
    {
        public CategoryUC()
        {
            InitializeComponent();
        }

        #region Variables
        private Logger logger = LogManager.GetCurrentClassLogger();
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        public Category categoryItem { get; set; }
        public event EventHandler EventCloseCategoryUC;
        public event EventHandler EventOpenSubCategItem;
        #endregion

        private void btnBack_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            Storyboard ClosePage_SB = TryFindResource("ClosePage_SB") as Storyboard;
            ClosePage_SB.Completed += new EventHandler(ClosePage_SB_Completed);
            ClosePage_SB.Begin();
            logger.Info("Completed");
        }

        void ClosePage_SB_Completed(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            EventCloseCategoryUC(this, null);
            logger.Info("Completed");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            if (categoryItem != null)
            {
                this.DataContext = categoryItem;
                ICCategory.ItemsSource = categoryItem.LstSubCategories.Where(c => c.IsActive);
                stkRightSlide.Visibility = categoryItem.LstSubCategories.Where(c => c.IsActive).ToList().Count > 5 ? Visibility.Visible : Visibility.Collapsed;
            }
            logger.Info("Completed");
        }

        private void btnSubCategItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {

                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }

        private void svCategory_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }

            if (svCategory.HorizontalOffset > 230)
            {
                stkLeftSlide.Visibility = Visibility.Visible;
                stkRightSlide.Visibility = Visibility.Visible;
            }
            if (svCategory.HorizontalOffset == svCategory.ScrollableWidth)
            {
                stkRightSlide.Visibility = Visibility.Collapsed;
                stkLeftSlide.Visibility = Visibility.Visible;
            } 
            if (svCategory.HorizontalOffset == 0)
            {
                stkLeftSlide.Visibility = Visibility.Collapsed;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svCategory_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCategory_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCategory_TouchDown(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    logger.Info("Initiated");
                    isScrollMoved = false;
                    EventOpenSubCategItem(btnTouchedItem, null);
                    logger.Info("Completed");
                }
            }
        }

        private void svCategory_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        public void Dispose()
        {
            btnTouchedItem = null;
            touchScrollStartPoint = null;
            touchScrollEndPoint = null;
            categoryItem = null;
            EventCloseCategoryUC = null;
            EventOpenSubCategItem = null;
        }
    }
}
