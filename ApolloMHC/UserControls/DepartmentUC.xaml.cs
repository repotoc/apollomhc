﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for DepartmentUC.xaml
    /// </summary>
    public partial class DepartmentUC : UserControl, IDisposable
    {
        public DepartmentUC()
        {
            InitializeComponent();
        }
        #region Variables
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        public event EventHandler EventCloseDepartmentUC;
        public event EventHandler EventOpenTeamUC;
        private Logger logger = LogManager.GetCurrentClassLogger();
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            List<Department> lstDepartment = Apps.myDbContext.DEPARTMENTS.ToList();
            if (lstDepartment != null)
            {
                ICDepartment.ItemsSource = lstDepartment.Where(c => c.IsActive);
            }
            logger.Info("Completed");
        }

        private void btnCloseTeam_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            Storyboard ClosePage_SB = TryFindResource("ClosePage_SB") as Storyboard;
            ClosePage_SB.Completed += new EventHandler(ClosePage_SB_Completed);
            ClosePage_SB.Begin();
            logger.Info("Completed");
        }

        void ClosePage_SB_Completed(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            EventCloseDepartmentUC(this, null);
            logger.Info("Completed");  
        }

        private void svDepartment_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnDepartment_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {

                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }

        private void svDepartment_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svDepartment_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svDepartment_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svDepartment_TouchDown(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    logger.Info("Initiated");
                    isScrollMoved = false;
                    Department department = btnTouchedItem.DataContext as Department;
                    if (department != null)
                    {
                        EventOpenTeamUC(department, null);
                    }
                    logger.Info("Completed");
                }
            }
        }

        public void Dispose()
        {
            btnTouchedItem = null;
            touchScrollStartPoint = null;
            touchScrollEndPoint = null;
            EventCloseDepartmentUC = null;
            EventOpenTeamUC = null;
        }
    }
}
