﻿using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for VideoView.xaml
    /// </summary>
    public partial class VideoView : UserControl, IDisposable
    {
        public VideoView()
        {
            InitializeComponent();
        }
        #region variables
        private Logger logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseVideoView;
        public DocumentDetails documentDets { get; set; }
        public string mediaPath { get; set; } 
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            if (documentDets != null)
            {
                this.DataContext = documentDets;
                mediaPath = documentDets.FileLocation;
                if (!string.IsNullOrEmpty(mediaPath))
                {
                    MEVideo.Source = new System.Uri(mediaPath);
                    MEVideo.Play();
                    MEVideo.Position = new TimeSpan(0, 0, 2);
                }
                else
                    EventCloseVideoView(this, null);
            }
            logger.Info("Completed");
        }
        private void MEVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            //close video on media end
            EventCloseVideoView(this, null);
            logger.Info("Completed");
        }

        private void btnCloseButton_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventCloseVideoView(this, null);
            logger.Info("Completed");
        }

        private void btnPlay_TouchDown(object sender, TouchEventArgs e)
        {
            MEVideo.Play();
            btnPause.Visibility = Visibility.Visible;
            btnPlay.Visibility = Visibility.Collapsed;
        }

        private void btnPause_TouchDown(object sender, TouchEventArgs e)
        {
            MEVideo.Pause();
            btnPause.Visibility = Visibility.Collapsed;
            btnPlay.Visibility = Visibility.Visible;
        }

        private void btnStop_TouchDown(object sender, TouchEventArgs e)
        {
            MEVideo.Stop();
            MEVideo.Position = new TimeSpan(0, 0, 2);
            btnPlay.Visibility = Visibility.Visible;
            btnPause.Visibility = Visibility.Collapsed;
        }

        public void Dispose()
        {
            EventCloseVideoView = null;
            documentDets = null;
            this.DataContext = null;
        }
    }
}
