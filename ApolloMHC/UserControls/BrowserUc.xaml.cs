﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for BrowserUc.xaml
    /// </summary>
    public partial class BrowserUc : UserControl, IDisposable
    {
        #region variables
        private Logger logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseBrowserUc;
        #endregion
        public BrowserUc()
        {
            InitializeComponent();
        }

        private void btnBrowserClose_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            GridBrowserLayout.Children.Clear();
            EventCloseBrowserUc(this, null);
            Apps.KillOSK();
            logger.Info("Completed");
        }

        private void btnKeyboard_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        public void Dispose()
        {
            Apps.KillOSK();
            EventCloseBrowserUc = null;
        }
    }
}
