﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for ImageView.xaml
    /// </summary>
    public partial class ImageView : UserControl, IDisposable
    {
        public ImageView()
        {
            InitializeComponent();
        }
        #region variables
        public DocumentDetails documentDets { get; set; }
        public event EventHandler EventCloseImageView;
        private Logger logger = LogManager.GetCurrentClassLogger(); 
        #endregion

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventCloseImageView(this, null);
            logger.Info("Completed");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            this.DataContext = documentDets;
            logger.Info("Completed");
        }

        public void Dispose()
        {
            documentDets = null;
            this.DataContext = null;
            EventCloseImageView = null;
        }
    }
}
