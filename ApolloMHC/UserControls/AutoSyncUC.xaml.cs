﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using ApolloSync.InitiateCalls;
using ApolloSync.Logic;
using ApolloSync.Logic.Constants;
using System.IO;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for AutoSyncUC.xaml
    /// </summary>
    public partial class AutoSyncUC : UserControl, IDisposable
    {
        #region variables
        private BackgroundWorker worker;
        public event EventHandler EventCloseSetAutoSync;
        private Logger logger = LogManager.GetCurrentClassLogger();
        #endregion
        public AutoSyncUC()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.RunWorkerAsync();
            logger.Info("Completed");
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            logger.Info("Initiated");
            Apps.myDbContext = new ApolloDB.DBFolder.ApolloDBContext();
            Storyboard ClosePage_SB = TryFindResource("ClosePage_SB") as Storyboard;
            ClosePage_SB.Completed += ClosePage_SB_Completed;
            ClosePage_SB.Begin();
            logger.Info("Completed");
        }

        void ClosePage_SB_Completed(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            EventCloseSetAutoSync(this, null);
            logger.Info("Completed");
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            logger.Info("Initiated");
            ProgressInformation progress = e.UserState as ProgressInformation;

            if (progress != null)
            {
                pbSplashScreen.Minimum = progress.StartIndex;
                pbSplashScreen.Maximum = progress.LastIndex;
                pbText.Text = progress.Message;
                pbSplashScreen.Value = e.ProgressPercentage;
            }
            logger.Info("Completed");
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            logger.Info("Initiated");
            AutoSync autoSync = new AutoSync();
            autoSync.syncProgressEvent += autoSync_syncProgressEvent;
            autoSync.StartDBSync();

            worker.ReportProgress(90, new ProgressInformation("Deleting Home SlideShow from Local Directory.", string.Empty, 1, 90, 100));
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + ApolloSyncImageFolderStructure.HomeSlideShow;
                if (Directory.Exists(path) && Directory.GetFiles(path).Count() > 0)
                {
                    List<string> homeSlideShowImgIds = Apps.myDbContext.HOMESLIDESHOW.Select(c => c.IMAGEID).ToList();
                    List<string> lstFileNamePaths = Directory.GetFiles(path).ToList();
                    List<string> lstFileNames = new List<string>();
                    lstFileNamePaths.ForEach(c =>
                        {
                            lstFileNames.Add(System.IO.Path.GetFileNameWithoutExtension(c));
                        });

                    List<string> lstDeleteFiles = lstFileNames.Where(c => !homeSlideShowImgIds.Contains(c)).ToList();
                    lstDeleteFiles.ForEach(c =>
                        {
                            string deletefile = lstFileNamePaths.FirstOrDefault(d => d.Contains(c));
                            if (!string.IsNullOrEmpty(deletefile))
                            {
                                try
                                {
                                    File.Delete(deletefile);
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex, ex.Message);
                                }
                            }
                        });
                }
                logger.Info("Completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }

        void autoSync_syncProgressEvent(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            if (sender is ProgressInformation)
            {
                ProgressInformation progress = (ProgressInformation)sender;
                worker.ReportProgress(progress.CurrentIndex, progress);
            }
            logger.Info("Completed");
        }

        public void Dispose()
        {
            worker = null;
            EventCloseSetAutoSync = null;
        }
    }
}
