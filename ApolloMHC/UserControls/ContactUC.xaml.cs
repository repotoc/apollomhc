﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using Microsoft.Maps.MapControl.WPF;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for ContactUC.xaml
    /// </summary>
    public partial class ContactUC : UserControl, IDisposable
    {
        public ContactUC()
        {
            InitializeComponent();
        }
        #region variables
        public event EventHandler EventCloseContactUs;
        private Logger logger = LogManager.GetCurrentClassLogger(); 
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            List<ContactUS> lstContacts = Apps.myDbContext.CONTACTUS.ToList();
            ICContactUS.ItemsSource = lstContacts;
            CreatePushPins(lstContacts);
            logger.Info("Completed");
        }
        private void CreatePushPins(List<ContactUS> lstContacts)
        {
            logger.Info("Initiated");
            if (lstContacts != null)
            {
                lstContacts.ToList().ForEach(c =>
                {
                    Pushpin pin = CreatePushPin(c);
                    if (pin != null)
                    {
                        map.Children.Add(pin);
                    }
                });
            }
            logger.Info("Completed");
        }

        private Pushpin CreatePushPin(ContactUS locationData)
        {
            logger.Info("Initiated");
            if (locationData != null && locationData.Location != null)
            {
                Pushpin pin = new Pushpin();
                pin.Location = new Location(Convert.ToDouble(locationData.Location.Latitude), Convert.ToDouble(locationData.Location.Longitude));
                return pin;
            }
            logger.Info("Completed");
            return null;
        }

        private void btnBack_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            Storyboard ClosePage_SB = TryFindResource("ClosePage_SB") as Storyboard;
            ClosePage_SB.Completed -= new EventHandler(ClosePage_SB_Completed);
            ClosePage_SB.Completed += new EventHandler(ClosePage_SB_Completed);
            ClosePage_SB.Begin();
            logger.Info("Completed");
        }

        void ClosePage_SB_Completed(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            EventCloseContactUs(this, null);
            logger.Info("Completed");
        }

        private void svContactLocation_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        public void Dispose()
        {
            EventCloseContactUs = null;
        }
    }
}
