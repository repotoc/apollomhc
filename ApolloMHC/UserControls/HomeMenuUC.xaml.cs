﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using ApolloDB.DBFolder;
using System.Windows.Threading;
using System.Diagnostics;
using NLog;

namespace ApolloMHC.UserControls
{
    /// <summary>
    /// Interaction logic for HomeMenuUC.xaml
    /// </summary>
    public partial class HomeMenuUC : UserControl, IDisposable
    {
        public HomeMenuUC()
        {
            InitializeComponent();
        }

        #region variables
        public event EventHandler EventOpenFeedback;
        public event EventHandler EventLoadCategoryUC;
        public event EventHandler EventLoadLink;
        private Logger logger = LogManager.GetCurrentClassLogger(); 
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            ItemsContext1.ItemsSource = Apps.myDbContext.CATEGORY.ToList().Where(c => c.ColumnNbr.Equals(1) && c.IsActive).OrderBy(c => c.Order);
            ItemsContext2.ItemsSource = Apps.myDbContext.CATEGORY.ToList().Where(c => c.ColumnNbr.Equals(2) && c.IsActive).OrderBy(c => c.Order);
            ItemsContext3.ItemsSource = Apps.myDbContext.CATEGORY.ToList().Where(c => c.ColumnNbr.Equals(3) && c.IsActive).OrderBy(c => c.Order);
            ItemsContext4.ItemsSource = Apps.myDbContext.CATEGORY.ToList().Where(c => c.ColumnNbr.Equals(4) && c.IsActive).OrderBy(c => c.Order);
            logger.Info("Completed");
        }
        
       
        private void btnFeedback_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EventOpenFeedback(this, null);
            logger.Info("Completed");
        }

        private void btnItem_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            Button btn = sender as Button;
            Category category = btn.DataContext as Category;
            if (category != null)
            {
                EventLoadCategoryUC(category, null);
            }
            logger.Info("Completed");
        }

        private void btnWebLink_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            Button btn = sender as Button;
            if (btn != null)
            {
                string url;
                switch (btn.Name)
                {
                    case "btnApolloPrism":
                        url = "http://www.apolloprism.com";
                        EventLoadLink(url, null);
                        break;
                    case "btnStressScreening":
                        url = "http://10.40.63.88/screentest/Admin/Form5.aspx";
                        EventLoadLink(url, null);
                        break;
                    default: break;
                }
            }
            logger.Info("Completed");
        }

        public void Dispose()
        {
            EventOpenFeedback = null;
            EventLoadCategoryUC = null;
            EventLoadLink = null;

            ItemsContext1.ItemsSource = null;
        }
    }
}
