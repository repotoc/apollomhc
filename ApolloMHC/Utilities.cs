﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApolloMHC
{
    public class Utilities
    {
        public static string SlideShowServerUrl
        {
            get { return ConfigurationManager.AppSettings["SlideShowserverUrl"]; }
        }

        public static string SlideShowHostedService
        {
            get { return ConfigurationManager.AppSettings["SlideShowHostedService"]; }
        }

        public static string HostedService
        {
            get { return ConfigurationManager.AppSettings["HostedService"]; }
        }
        public static string CmcLoginService
        {
            get { return ConfigurationManager.AppSettings["CMCLoginService"]; }
        }
        public static string UserName
        {
            get { return ConfigurationManager.AppSettings["UserName"]; }
        }

        public static string Password
        {
            get { return ConfigurationManager.AppSettings["Password"]; }
        }
    }
}
