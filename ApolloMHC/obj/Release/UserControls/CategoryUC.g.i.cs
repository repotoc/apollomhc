﻿#pragma checksum "..\..\..\UserControls\CategoryUC.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "55B48A61C5BCE4330BD396FD711F8536"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ApolloMHC.UserControls {
    
    
    /// <summary>
    /// CategoryUC
    /// </summary>
    public partial class CategoryUC : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 37 "..\..\..\UserControls\CategoryUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid MainLayout;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\UserControls\CategoryUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBack;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\UserControls\CategoryUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbCategoryName;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\UserControls\CategoryUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgBanner;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\UserControls\CategoryUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer svCategory;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\UserControls\CategoryUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl ICCategory;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\UserControls\CategoryUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkLeftSlide;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\UserControls\CategoryUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkRightSlide;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ApolloMHC;component/usercontrols/categoryuc.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\CategoryUC.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 6 "..\..\..\UserControls\CategoryUC.xaml"
            ((ApolloMHC.UserControls.CategoryUC)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 3:
            this.MainLayout = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.btnBack = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\..\UserControls\CategoryUC.xaml"
            this.btnBack.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnBack_TouchDown);
            
            #line default
            #line hidden
            return;
            case 5:
            this.tbCategoryName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.imgBanner = ((System.Windows.Controls.Image)(target));
            return;
            case 7:
            this.svCategory = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 61 "..\..\..\UserControls\CategoryUC.xaml"
            this.svCategory.ScrollChanged += new System.Windows.Controls.ScrollChangedEventHandler(this.svCategory_ScrollChanged);
            
            #line default
            #line hidden
            
            #line 62 "..\..\..\UserControls\CategoryUC.xaml"
            this.svCategory.PreviewTouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.svCategory_PreviewTouchDown);
            
            #line default
            #line hidden
            
            #line 63 "..\..\..\UserControls\CategoryUC.xaml"
            this.svCategory.PreviewTouchMove += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.svCategory_PreviewTouchMove);
            
            #line default
            #line hidden
            
            #line 64 "..\..\..\UserControls\CategoryUC.xaml"
            this.svCategory.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.svCategory_PreviewTouchUp);
            
            #line default
            #line hidden
            
            #line 65 "..\..\..\UserControls\CategoryUC.xaml"
            this.svCategory.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.svCategory_ManipulationBoundaryFeedback);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ICCategory = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 9:
            this.stkLeftSlide = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 10:
            this.stkRightSlide = ((System.Windows.Controls.StackPanel)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 2:
            
            #line 17 "..\..\..\UserControls\CategoryUC.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnSubCategItem_TouchDown);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

