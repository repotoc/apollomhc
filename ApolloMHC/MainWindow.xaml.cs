﻿using ApolloMHC.AdminUserControls;
using ApolloMHC.UserControls.AlertMessageControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloMHC.UserControls;
using System.Windows.Threading;
using System.Configuration;
using System.Reflection;
using ApolloDB.Models;
using System.IO;
using System.ComponentModel;
using NLog;
using CefSharp.Wpf;
using CefSharp;
using ApolloMHC.Enums;

namespace ApolloMHC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region variables
        private DispatcherTimer idleTimer, autoSyncTimer;
        private int idleTimerSeconds = 0;
        private Configuration config;
        public static int zIndexValue = 9999;
        public static int ivRenTrsf;

        AdminSettings AdmSetting;
        ManipulationModes currentMode = ManipulationModes.All;
        private Logger logger = LogManager.GetCurrentClassLogger();
        public ChromiumWebBrowser browser;
        #endregion
        public MainWindow()
        {
            InitializeComponent();
            InitiateBrowser();
            ivRenTrsf = 10;
        }
        private void InitiateBrowser()
        {
            Cef.Initialize(new CefSettings());
            browser = new ChromiumWebBrowser();
        }

        #region Methods
        private int SetZIndexValue()    //increment ZIndex value method
        {
            return zIndexValue++;
        }
        private void CheckRenderTransform()
        {
            int countIV = 0;
            foreach (Control ctrl in GridPopupPanel.Children)
            {
                if (ctrl is ImageView || ctrl is VideoView || ctrl is BroucherUC)
                    countIV++;
            }
            if (ivRenTrsf > 120 || countIV == 0)
                ivRenTrsf = 20;
            else
            {
                ivRenTrsf += 20;
            }
        }
        private void LoadHomePage()
        {
            logger.Info("Initiated");
            RemoveAllGridUserControlPanel();
            RemoveAllGridPopupPanel();
            RemoveAllGridAlertControlPanel();
            RemoveAllGridAdminSettingsPanel();

            zIndexValue = 9999;
            SlideShowUC slide = new SlideShowUC();
            slide.BringIntoView();
            Panel.SetZIndex(slide, SetZIndexValue());
            GridUserControlPanel.Children.Add(slide);
            slide.EventCloseSlideShow -= slide_EventCloseSlideShow;
            slide.EventCloseSlideShow += slide_EventCloseSlideShow;

            slide.EventOpenAdminSettings -= slide_EventOpenAdminSettings;
            slide.EventOpenAdminSettings += slide_EventOpenAdminSettings;
            idleTimer.Stop();
            logger.Info("Completed");
        }

        void sync_EventCloseSetAutoSync(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            //remove autosyncuc from Gridsynccontrolpanel after sync
            AutoSyncUC sync = (AutoSyncUC)sender;
            GridSyncControlPanel.Children.Remove(sync);
            logger.Info("Completed");
        }
        private void HomePage_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
            {
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);
            }

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }
        private void HomePage_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            if (element != null)
            {
                MatrixTransform xform = element.RenderTransform as MatrixTransform;
                Matrix matrix = xform.Matrix;
                ManipulationDelta delta = args.DeltaManipulation;
                Point center = args.ManipulationOrigin;
                matrix.Translate(-center.X, -center.Y);
                matrix.Scale(delta.Scale.X, delta.Scale.Y);
                matrix.Translate(center.X, center.Y);
                matrix.Translate(delta.Translation.X, delta.Translation.Y);
                xform.Matrix = matrix;
            }
            args.Handled = true;
            base.OnManipulationDelta(args);
        }

        #endregion

        #region TeamUserControl
        private void btnTeam_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            LoadDepartment();
            logger.Info("Completed");
        }

        private void LoadDepartment()
        {
            logger.Info("Initiated");
            if (!CheckDepartment())
            {
                DepartmentUC dept = new DepartmentUC();
                dept.BringIntoView();
                Panel.SetZIndex(dept, SetZIndexValue());
                GridUserControlPanel.Children.Add(dept);
                //raise events of DepartmentUC
                dept.EventCloseDepartmentUC -= dept_EventCloseDepartmentUC;
                dept.EventCloseDepartmentUC += dept_EventCloseDepartmentUC;

                dept.EventOpenTeamUC -= dept_EventOpenTeamUC;
                dept.EventOpenTeamUC += dept_EventOpenTeamUC;
            }
            logger.Info("Completed");
        }

        private bool CheckDepartment()
        {
            bool isExists = false;
            foreach (UIElement item in GridUserControlPanel.Children)    //check weather DepartmentUC already exists
            {
                if (item is DepartmentUC)
                {
                    Panel.SetZIndex(item, SetZIndexValue());
                    logger.Info("Already Exists");
                    isExists = true;
                }
            }
            return isExists;
        }
        void dept_EventOpenTeamUC(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            Department department = sender as Department;
            if (department != null)
            {
                if (department.LstDoctors != null && department.LstDoctors.Count > 0)
                {
                    LoadTeamUC(department);
                }
                else
                {
                    LoadUnderDevelopmentUC();
                }
            }
            logger.Info("Completed");
        }

        void dept_EventCloseDepartmentUC(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridUserControlPanel(Panels.DepartmentUC);
            logger.Info("Completed");
        }

        private void LoadTeamUC(Department department)
        {
            logger.Info("Initiated");
            Apps.InsertPageHit("Team Page");
            
            if (!CheckTeamUc())
            {
                TeamUC team = new TeamUC();
                team.department = department;
                team.RenderTransform = new MatrixTransform(1, 0, 0, 1, 1, 1);
                team.BringIntoView();
                Panel.SetZIndex(team, SetZIndexValue());
                GridUserControlPanel.Children.Add(team);
                team.EventCloseTeam -= team_EventCloseTeam;
                team.EventCloseTeam += team_EventCloseTeam;
            }
            logger.Info("Completed");
        }

        private bool CheckTeamUc()
        {
            bool isExists = false;
            foreach (UIElement item in GridUserControlPanel.Children)
            {
                if (item is TeamUC)
                {
                    Panel.SetZIndex(item, SetZIndexValue());
                    logger.Info("Already exists");
                    isExists = true;
                }
            }
            return isExists;
        }

        

        void team_EventCloseTeam(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridUserControlPanel(Panels.TeamUC);
            logger.Info("Completed");
        }
        #endregion


        #region ContactUserControl

        private void btnContactUs_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            Apps.InsertPageHit("Contact Us Page");
            
            if (!CheckContactUs())
            {
                //load ContactUC page
                ContactUC contact = new ContactUC();
                contact.RenderTransform = new MatrixTransform(1, 0, 0, 1, 1, 1);
                contact.BringIntoView();
                Panel.SetZIndex(contact, SetZIndexValue());
                GridUserControlPanel.Children.Add(contact);

                contact.EventCloseContactUs -= contact_EventCloseContactUs;
                contact.EventCloseContactUs += contact_EventCloseContactUs;
            }
            logger.Info("Completed");
        }

        private bool CheckContactUs()
        {
            bool isExists = false;
            foreach (UIElement item in GridUserControlPanel.Children)
            {
                if (item is ContactUC)
                {
                    Panel.SetZIndex(item, SetZIndexValue());
                    logger.Info("Already Exists");
                    isExists = true;
                }
            }
            return isExists;
        }

        void contact_EventCloseContactUs(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridUserControlPanel(Panels.ContactUC);
            logger.Info("Completed");
        }
        #endregion

        #region AlertMessageMethods
        private void app_WarningAlert(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            string infoMsg = (sender is string) ? (string)sender : string.Empty;
            WarningAlertUC wAlert = new WarningAlertUC();
            wAlert.tblWarningText.Text = infoMsg;
            GridAlertControlPanel.Children.Add(wAlert);
            wAlert.EventCloseWarningAlert -= wAlert_EventCloseWarningAlert;
            wAlert.EventCloseWarningAlert += wAlert_EventCloseWarningAlert;
            logger.Info("Completed");
        }

        void wAlert_EventCloseWarningAlert(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridAlertControlPanel(Panels.WarningAlertUC);
            logger.Info("Completed");
        }

        private void app_ErrorAlert(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            string infoMsg = (sender is string) ? (string)sender : string.Empty;
            ErrorAlertUC eAlert = new ErrorAlertUC();
            string message = sender as string;
            eAlert.tblErrorMessage.Text = message;
            GridAlertControlPanel.Children.Add(eAlert);
            //raise events here
            eAlert.EventCloseErrorAlert += eAlert_EventCloseErrorAlert;
            logger.Info("Completed");
        }

        void eAlert_EventCloseErrorAlert(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridAlertControlPanel(Panels.ErrorAlertUC);
            logger.Info("Completed");
        }

        private void app_SuccessAlert(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            string message = (sender is string) ? (string)sender : string.Empty;
            SuccessAlertUC sAlert = new SuccessAlertUC();
            sAlert.tblSuccessMessage.Text = message;
            GridAlertControlPanel.Children.Add(sAlert);
            sAlert.EventCloseSuccessAlert -= sAlert_EventCloseSuccessAlert;
            sAlert.EventCloseSuccessAlert += sAlert_EventCloseSuccessAlert;
            logger.Info("Completed");
        }

        void sAlert_EventCloseSuccessAlert(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridAlertControlPanel(Panels.SuccessAlertUC);
            logger.Info("Completed");
        }


        private void LoadUnderDevelopmentUC()
        {
            logger.Info("Initiated");
            UnderDevelopmentUC dev = new UnderDevelopmentUC();
            GridAlertControlPanel.Children.Add(dev);
            dev.EventCloseDevelopmentUC -= dev_EventCloseDevelopmentUC;
            dev.EventCloseDevelopmentUC += dev_EventCloseDevelopmentUC;
            logger.Info("Completed");
        }

        void dev_EventCloseDevelopmentUC(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridAlertControlPanel(Panels.UnderDevelopmentUC);
            logger.Info("Completed");
        }
        #endregion


        #region Admin Panel

        private void LoadAdminLogin()
        {
            logger.Info("Initiated");
            if (!CheckAdminLogin())
            {
                RemoveAllGridAdminSettingsPanel();

                AdminLogin AdmLogin = new AdminLogin();
                GridAdminSettingsPanel.Children.Add(AdmLogin);

                AdmLogin.EventCloseAdminSetting -= AdmLogin_EventCloseAdminSetting;
                AdmLogin.EventCloseAdminSetting += AdmLogin_EventCloseAdminSetting;

                AdmLogin.EventLoginAdmin -= AdmLogin_EventLoginAdmin;
                AdmLogin.EventLoginAdmin += AdmLogin_EventLoginAdmin;

                AdmLogin.EventWarningAlert -= app_WarningAlert;
                AdmLogin.EventWarningAlert += app_WarningAlert;
            }
            logger.Info("Completed");
        }

        private bool CheckAdminLogin()
        {
            bool isExists = false;
            foreach (UIElement item in GridAdminSettingsPanel.Children)
            {
                if (item is AdminLogin)
                {
                    logger.Info("Already Exists");
                    isExists = true;
                }
            }
            return isExists;
        }

        void AdmLogin_EventCloseAdminSetting(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridAdminSettingsPanel(Panels.AdminLogin);
            logger.Info("Completed");
        }
        
        void AdmLogin_EventLoginAdmin(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            LoadAdminSettings();
            logger.Info("Completed");
        }

        private void LoadAdminSettings()
        {
            logger.Info("Initiated");
            bool isExists = false;
            foreach (UIElement item in GridAdminSettingsPanel.Children)
            {
                if (item is AdminSettings)
                {
                    logger.Info("Already Exists");
                    isExists = true;
                }
            }
            if (!isExists)
            {
                //load AdminSettings
                AdmSetting = new AdminSettings();
                GridAdminSettingsPanel.Children.Clear();
                GridAdminSettingsPanel.Children.Add(AdmSetting);
                //load events in AdminSettings
                AdmSetting.EventLogoutSettings += AdmSetting_EventLogoutSettings;
                AdmSetting.EventSlideShowIdleTimer += AdmSetting_EventSlideShowIdleTimer;
                AdmSetting.EventOpenSetAutoSync += admSettings_EventAutoSync;
                AdmSetting.EventEmailConfiguration += AdmSetting_EventEmailConfiguration;
            }
            logger.Info("Completed");
        }

        void AdmSetting_EventEmailConfiguration(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            GridAdminSettingsPanel.Children.Clear();
            EmailConfigurationUC emailConfig = new EmailConfigurationUC();
            GridAdminSettingsPanel.Children.Add(emailConfig);
            //raise events here
            emailConfig.EventCloseEmailSetting -= emailConfig_EventCloseEmailSetting;
            emailConfig.EventCloseEmailSetting += emailConfig_EventCloseEmailSetting;

            emailConfig.EventSuccessAlert -= app_SuccessAlert;
            emailConfig.EventSuccessAlert += app_SuccessAlert;
            
            emailConfig.EventWarningAlert -= app_WarningAlert;
            emailConfig.EventWarningAlert += app_WarningAlert;
            
            emailConfig.EventErrorAlert -= app_ErrorAlert;
            emailConfig.EventErrorAlert += app_ErrorAlert;
            logger.Info("Completed");
        }

        void emailConfig_EventCloseEmailSetting(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridAdminSettingsPanel(Panels.EmailConfigurationUC);
            LoadAdminSettings();
            logger.Info("Completed");
        }


        #region AutoSyncRegion
        private void InitiateAutoSync()
        {
            logger.Info("Initiated");
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                if (Convert.ToBoolean(config.AppSettings.Settings["AutoSyncEnabled"].Value))
                {
                    DateTime autoSyncTime = DateTime.Now;

                    if (DateTime.TryParse(config.AppSettings.Settings["AutoSyncTiming"].Value, out autoSyncTime))
                    {
                        string syncTime = autoSyncTime.ToString("HH:mm");
                        if (autoSyncTimer != null)
                        {
                            autoSyncTimer.Stop();
                            autoSyncTimer = null;
                        }
                        DateTime fromDate = DateTime.Now;
                        DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + syncTime);

                        TimeSpan tspan = fetchSyncDate - fromDate;

                        if (tspan.Seconds < 0)
                        {
                            fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + syncTime);
                            tspan = fetchSyncDate - fromDate;
                        }
                        if (autoSyncTimer == null)
                        {
                            autoSyncTimer = new DispatcherTimer();
                            autoSyncTimer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                            autoSyncTimer.Tick += autoSyncTimer_Tick;
                            autoSyncTimer.Start();
                        }
                    }
                }
                logger.Info("Completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }
        void autoSyncTimer_Tick(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            if (!CheckAutosyncExists())
            {
                AutoSyncUC sync = new AutoSyncUC();
                GridSyncControlPanel.Children.Add(sync);
                sync.EventCloseSetAutoSync -= autosync_EventCloseSetAutoSync;
                sync.EventCloseSetAutoSync += autosync_EventCloseSetAutoSync;
            }
            autoSyncTimer.Stop();
            logger.Info("Completed");
        }
        
        private bool CheckAutosyncExists()  //method to check autosyncuc already exists or not
        {
            logger.Info("Initiated");
            foreach (UIElement item in GridSyncControlPanel.Children)
            {
                if (item is AutoSyncUC)
                {
                    logger.Info("Completed Returns True");
                    return true;
                }
            }
            logger.Info("Completed Returns False");
            return false;
        }

        #endregion

        #region AutoSync
        void admSettings_EventAutoSync(object sender, EventArgs e)
        {
            GridAdminSettingsPanel.Children.Clear();
            SetAutoSyncTime autosync = new SetAutoSyncTime();
            GridAdminSettingsPanel.Children.Add(autosync);
            autosync.autoSyncRefresh -= autosync_autoSyncRefresh;
            autosync.autoSyncRefresh += autosync_autoSyncRefresh;

            autosync.EventSyncNow -= autosync_EventSyncNow;
            autosync.EventSyncNow += autosync_EventSyncNow;

            autosync.EventSuccessAlert -= app_SuccessAlert;
            autosync.EventSuccessAlert += app_SuccessAlert;

            autosync.EventErrorAlert -= app_ErrorAlert;
            autosync.EventErrorAlert += app_ErrorAlert;

            autosync.EventCloseSetAutoSync -= autosync_EventCloseSetAutoSync;
            autosync.EventCloseSetAutoSync += autosync_EventCloseSetAutoSync;
        }
        private void InitiateSync()
        {
            logger.Info("Initiated");
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);

                DateTime autoSyncTime = DateTime.Now.AddMilliseconds(10);

                string syncTime = autoSyncTime.ToString("HH:mm");
                if (autoSyncTimer != null)
                {
                    autoSyncTimer.Stop();
                    autoSyncTimer = null;
                }
                DateTime fromDate = DateTime.Now;
                DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + syncTime);

                TimeSpan tspan = fetchSyncDate - fromDate;

                if (tspan.Seconds < 0)
                {
                    fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + syncTime);
                    tspan = new TimeSpan(0, 0, 1); // fetchSyncDate - fromDate;
                }
                if (autoSyncTimer == null)
                {
                    autoSyncTimer = new DispatcherTimer();
                    autoSyncTimer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                    autoSyncTimer.Tick += autoSyncTimer_Tick;
                    autoSyncTimer.Start();
                }
                logger.Info("Completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }

        void autosync_autoSyncRefresh(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            InitiateAutoSync();
            logger.Info("Completed");
        }
        void autosync_EventSyncNow(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            InitiateSync();
            logger.Info("Completed");
        }

        void autosync_EventCloseSetAutoSync(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridAdminSettingsPanel(Panels.SetAutoSyncTime);
            LoadAdminSettings();
            logger.Info("Completed");
        }

        #endregion

        void AdmSetting_EventLogoutSettings(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveAllGridAdminSettingsPanel();
            logger.Info("Completed");
        }
        void AdmSetting_EventSlideShowIdleTimer(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveAllGridAdminSettingsPanel();

            SlideShowIdleTimer ssIdleTimer = new SlideShowIdleTimer();
            GridAdminSettingsPanel.Children.Add(ssIdleTimer);

            ssIdleTimer.EventCloseIdleTimer -= ssIdleTimer_EventCloseIdleTimer;
            ssIdleTimer.EventCloseIdleTimer += ssIdleTimer_EventCloseIdleTimer;
            
            ssIdleTimer.EventSuccessAlert -= app_SuccessAlert;
            ssIdleTimer.EventSuccessAlert += app_SuccessAlert;
            
            ssIdleTimer.EventWarningAlert -= app_WarningAlert;
            ssIdleTimer.EventWarningAlert += app_WarningAlert;
            logger.Info("Completed");
        }

        void ssIdleTimer_EventCloseIdleTimer(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridAdminSettingsPanel(Panels.SlideShowIdleTimer);

            LoadAdminSettings();
            logger.Info("Completed");
        }

        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Initiated");
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            idleTimerSeconds = (!(Int32.TryParse(config.AppSettings.Settings["IdleTimer"].Value, out idleTimerSeconds)) && idleTimerSeconds <= 120) ? 120 : idleTimerSeconds;
            idleTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);
            idleTimer.Interval = new TimeSpan(0, 0, 5);
            idleTimer.Tick += idleTimer_Tick;
            InitiateAutoSync();
            LoadHomePage();

            #region Manipulation Events
            this.ManipulationStarting -= HomePage_ManipulationStarting;
            this.ManipulationDelta -= HomePage_ManipulationDelta;
            this.ManipulationStarting += HomePage_ManipulationStarting;
            this.ManipulationDelta += HomePage_ManipulationDelta;
            #endregion

            logger.Info("Completed");
        }


        void idleTimer_Tick(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            TimeSpan span = IdleTimeSetter.GetLastInput();
            if (idleTimerSeconds <= (Convert.ToInt32(span.TotalSeconds)))
            {
                RemoveAllGridUserControlPanel();
                LoadHomePage();
            }
            logger.Info("Completed");
        }

        void slide_EventOpenAdminSettings(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            LoadAdminLogin();
            logger.Info("Completed");
        }

        void slide_EventCloseSlideShow(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            SlideShowUC slide = (SlideShowUC)sender;
            GridUserControlPanel.Children.Remove(slide);
            GridUserControlPanel.Children.Clear();

            HomeMenuUC menu = new HomeMenuUC();
            GridUserControlPanel.Children.Add(menu);

            menu.EventOpenFeedback -= menu_EventOpenFeedback;
            menu.EventOpenFeedback += menu_EventOpenFeedback;

            menu.EventLoadCategoryUC -= menu_EventLoadCategoryUC;
            menu.EventLoadCategoryUC += menu_EventLoadCategoryUC;

            menu.EventLoadLink -= menu_EventLoadLink;
            menu.EventLoadLink += menu_EventLoadLink;
            idleTimer.Start();
            logger.Info("Completed");
        }

        private DispatcherTimer apolloWebTimer;
        private int webIdleTimerSeconds = 120;
        void menu_EventLoadLink(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            this.Dispatcher.Invoke((Action)(() =>
                {
                    if (Apps.PingCheck())
                    {
                        apolloWebTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);
                        apolloWebTimer.Interval = new TimeSpan(0, 0, 10);
                        apolloWebTimer.Tick += apolloWebTimer_Tick;
                        apolloWebTimer.IsEnabled = true;

                        string url = sender as string;
                        browser.Address = url;
                        BrowserUc browserUc = new BrowserUc();
                        browserUc.GridBrowserLayout.Children.Add(browser);
                        GridAlertControlPanel.Children.Add(browserUc);
                        browserUc.EventCloseBrowserUc -= browserUc_EventCloseBrowserUc;
                        browserUc.EventCloseBrowserUc += browserUc_EventCloseBrowserUc;

                        apolloWebTimer.Start();
                    }
                    else
                    {
                        InternetDown intDown = new InternetDown();
                        intDown.ShowDialog();
                    }
                }));

            logger.Info("Completed");
        }

        void apolloWebTimer_Tick(object sender, EventArgs e)
        {
            TimeSpan span = IdleTimeSetter.GetLastInput();
            if (webIdleTimerSeconds <= (Convert.ToInt32(span.TotalSeconds)))
            {
                try
                {
                    if (apolloWebTimer != null)
                    {
                        apolloWebTimer.Stop();
                        apolloWebTimer = null;
                    }
                    browser.CloseDevTools();
                    RemoveGridAlertControlPanel(Panels.BrowserUc);
                    //Process[] process = Process.GetProcesses();
                    //foreach (Process proc in process)
                    //{
                    //    switch (proc.ProcessName.ToLower())
                    //    {
                    //        case "osk": proc.Kill(); break;
                    //        case "firefox": proc.Kill(); break;
                    //        case "chrome": proc.Kill(); break;
                    //        case "iexplore": proc.Kill(); break;
                    //        case "microsoftedge": proc.Kill(); break;
                    //        case "tabtip": proc.Kill(); break;
                    //        default:
                    //            break;
                    //    }
                    //}
                }
                catch (Exception ex)
                {
                    logger.Error(ex, ex.Message);
                }
            }
        }


        void browserUc_EventCloseBrowserUc(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            browser.CloseDevTools();
            RemoveGridAlertControlPanel(Panels.BrowserUc);
            logger.Info("Completed");
        }

        void menu_EventLoadCategoryUC(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveAllGridPopupPanel();
            Category category = sender as Category;
            if (category != null && category.IsActive.Equals(true))
            {
                if (category.LstSubCategories != null && category.LstSubCategories.Count > 0)
                {
                    //Load category page
                    if (ValidateCategoryExists(category))
                    {
                        CategoryUC categUC = new CategoryUC();
                        categUC.categoryItem = category;
                        categUC.BringIntoView();
                        Panel.SetZIndex(categUC, SetZIndexValue());
                        GridUserControlPanel.Children.Add(categUC);
                        
                        categUC.EventCloseCategoryUC -= categUC_EventCloseCategoryUC;
                        categUC.EventCloseCategoryUC += categUC_EventCloseCategoryUC;

                        categUC.EventOpenSubCategItem -= categUC_EventOpenSubCategItem;
                        categUC.EventOpenSubCategItem += categUC_EventOpenSubCategItem;
                    }
                }
                else if (category.LstDocuments != null && category.LstDocuments.Count > 0)
                {
                    //load document page
                    List<Document> lstDocument = category.LstDocuments.Where(b => b.IsActive).ToList();
                    LoadDocument(lstDocument, category.Name);
                }
                else
                {
                    LoadUnderDevelopmentUC();
                }
            }
            logger.Info("Completed");
        }

        private bool ValidateCategoryExists(Category category)
        {
            logger.Info("Initiated");
            foreach (Control item in GridUserControlPanel.Children)
            {
                if(item is CategoryUC)
                {
                    item.BringIntoView();
                    Panel.SetZIndex(item, SetZIndexValue());
                    logger.Info("Already Exists");
                    return false;
                }
            }
            logger.Info("Completed");
            return true;
        }

        void categUC_EventCloseCategoryUC(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridUserControlPanel(Panels.CategoryUC);
            logger.Info("Completed");
        }

        void categUC_EventOpenSubCategItem(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            Button btnSubCategItem = sender as Button;
            if (btnSubCategItem != null)
            {
                SubCategory subCategItem = btnSubCategItem.DataContext as SubCategory;
                if (subCategItem != null)
                {
                    if (subCategItem.LstDocuments != null && subCategItem.LstDocuments.Count > 0)
                    {
                        List<Document> lstDocument = subCategItem.LstDocuments.Where(c => c.IsActive).ToList();
                        LoadDocument(lstDocument, subCategItem.Name);
                    }
                    else
                    {
                        LoadUnderDevelopmentUC();
                    }
                }
            }
            logger.Info("Completed");
        }

        private void LoadDocument(List<Document> lstDocument, string subCategName)
        {
            logger.Info("Initiated");
            string docType = string.Empty;
            if (lstDocument != null && lstDocument.Count == 1)
            {
                lstDocument.Where(b=>b.IsActive).ToList().ForEach(c =>
                {
                    docType = LoadByDocumentType(c);
                });

            }
            else if (lstDocument.Where(b => b.IsActive).Any(c => c.DocumentType.Equals("PPT") || c.DocumentType.Equals("PDF")))
            {
                LoadDocumentUC(lstDocument, subCategName);
            }
            else if (lstDocument.Count > 1 && lstDocument.Where(b => b.IsActive).Any(c => c.DocumentType.Equals("Video")))
            {
                LoadDocumentUC(lstDocument, subCategName);
            }
            else
            {
                lstDocument.Where(b => b.IsActive).ToList().ForEach(c =>
                {
                    docType = LoadByDocumentType(c);
                });
            }
            logger.Info("Completed");
        }

        private string LoadByDocumentType(Document document)
        {
            logger.Info("Initiated");
            string docType;
            docType = document.DocumentType;
            switch (docType)
            {
                case "Image":
                    LoadImageControl(document);
                    break;
                case "Video":
                    LoadVideoControl(document);
                    break;
                case "PPT":
                case "PDF":
                    LoadPresentationControl(document);
                    break;
                default:
                    break;
            }
            logger.Info("Completed");
            return docType;
        }

        private void LoadDocumentUC(List<Document> lstDocument, string subCategName)
        {
            logger.Info("Initiated");
            bool isExists = false;
            foreach (UIElement item in GridUserControlPanel.Children)   //check weather DocumentsUC already exists
            {
                if (item is DocumentsUC)
                {
                    logger.Info("Already Exists");
                    isExists = true;
                }
            }
            if (!isExists)
            {
                lstDocument = lstDocument.Where(c => c.LstDocDetails != null && c.LstDocDetails.Count > 0 && c.IsActive).ToList();
                if (lstDocument != null && lstDocument.Count > 0)
                {
                    DocumentsUC docUC = new DocumentsUC();
                    docUC.ICDocuments.ItemsSource = lstDocument;
                    docUC.btnBack.Content = subCategName;
                    docUC.BringIntoView();
                    Panel.SetZIndex(docUC, SetZIndexValue());
                    GridUserControlPanel.Children.Add(docUC);

                    docUC.EventOpenDocumentItem -= docUC_EventOpenDocumentItem;
                    docUC.EventOpenDocumentItem += docUC_EventOpenDocumentItem;

                    docUC.EventCloseDocumentUC -= docUC_EventCloseDocumentUC;
                    docUC.EventCloseDocumentUC += docUC_EventCloseDocumentUC;
                }
                else
                {
                    LoadUnderDevelopmentUC();
                }
            }
            logger.Info("Completed");
        }

        void docUC_EventCloseDocumentUC(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridUserControlPanel(Panels.DocumentsUC);
            logger.Info("Completed");
        }

        void docUC_EventOpenDocumentItem(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            Button btn = sender as Button;
            if (btn != null && btn.DataContext != null)
            {
                Document selectedDocument = btn.DataContext as Document;
                LoadByDocumentType(selectedDocument);
            }
            logger.Info("Completed");
        }

        #region LoadBroucherControlMethod
        private void LoadPresentationControl(Document selectedDocument)
        {
            logger.Info("Initiated");
            if (ValidateBroucher(selectedDocument))
            {
                //Load PresentationControl usercontrol
                if (selectedDocument != null && selectedDocument.LstDocDetails != null && selectedDocument.LstDocDetails.Count > 0)
                {
                    CheckRenderTransform();
                    BroucherUC broucher = new BroucherUC();
                    broucher.IsManipulationEnabled = true;
                    broucher.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                    broucher.BringIntoView();
                    Panel.SetZIndex(broucher, SetZIndexValue());
                    broucher.lstCategoryData = selectedDocument.LstDocDetails.Where(c => c.IsActive).ToList();
                    GridPopupPanel.Children.Add(broucher);
                    broucher.EventCloseBroucher -= broucher_EventCloseBroucher;
                    broucher.EventCloseBroucher += broucher_EventCloseBroucher;
                }
            }
            logger.Info("Completed");
        }

        private bool ValidateBroucher(Document docItem)
        {
            logger.Info("Initiated");
            int count = 0;
            foreach (Control item in GridUserControlPanel.Children)
            {
                if (item is BroucherUC)
                {
                    docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                    {
                        BroucherUC broucher = item as BroucherUC;
                        broucher.lstCategoryData.ForEach(d =>
                        {
                            if (d.ID.Equals(c.ID))
                            {
                                item.BringIntoView();
                                Panel.SetZIndex(item, SetZIndexValue());
                                count++;
                                logger.Info("Already Exists");
                            }
                        });
                    });
                }
            }
            if (count > 0)
            {
                logger.Info("Already Exists");
                return false;
            }
            else
            {
                logger.Info("Completed");
                return true;
            }
        }
        void broucher_EventCloseBroucher(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridPopupPanel(Panels.BroucherUC);
            logger.Info("Completed");
        }
        #endregion

        #region LoadImageControlMethod
        private void LoadImageControl(Document docItem)
        {
            logger.Info("Initiated");
            if (ValidateImageView(docItem))
            {
                docItem.LstDocDetails.Where(b=>b.IsActive).ToList().ForEach(c =>
                {
                    CheckRenderTransform();
                    ImageView imgView = new ImageView();
                    imgView.IsManipulationEnabled = true;
                    imgView.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                    imgView.BringIntoView();
                    imgView.documentDets = c;
                    Panel.SetZIndex(imgView, SetZIndexValue());
                    imgView.DataContext = c;
                    //imgView.img.Source = new BitmapImage(new Uri(c.FileLocation));
                    GridPopupPanel.Children.Add(imgView);
                    //raise events from ImageView
                    imgView.EventCloseImageView -= imgView_EventCloseImageView;
                    imgView.EventCloseImageView += imgView_EventCloseImageView;
                });
            }
            logger.Info("Completed");
        }

        private bool ValidateImageView(Document docItem)
        {
            logger.Info("Initiated");
            int count = 0;
            foreach (Control item in GridUserControlPanel.Children)
            {
                if (item is ImageView)
                {
                    docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                    {
                        DocumentDetails doc = item.DataContext as DocumentDetails;
                        if (doc.ID.Equals(c.ID))
                        {
                            item.BringIntoView();
                            Panel.SetZIndex(item, SetZIndexValue());
                            count++;
                        }
                    });
                }
            }
            if (count > 0)
            {
                logger.Info("Already Exists"); return false;
            }
            else
            {
                logger.Info("Completed");
                return true;
            }
        }

        void imgView_EventCloseImageView(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            //remove ImageView fro GridUserControlPanel
            ImageView imgView = (ImageView)sender;
            GridPopupPanel.Children.Remove(imgView);
            logger.Info("Completed");
        }
        #endregion

        #region LoadVideoControlMethod
        private void LoadVideoControl(Document docItem)
        {
            logger.Info("Initiated");
            //Load video usercontrol here
            if (ValidateVideoView(docItem))
            {
                docItem.LstDocDetails.Where(b=>b.IsActive).ToList().ForEach(c =>
                {
                    CheckRenderTransform();
                    VideoView vdoView = new VideoView();
                    vdoView.IsManipulationEnabled = true;
                    vdoView.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                    vdoView.BringIntoView();
                    Panel.SetZIndex(vdoView, SetZIndexValue());
                    vdoView.documentDets = c;
                    GridPopupPanel.Children.Add(vdoView);
                    //raise events from VideoView
                    vdoView.EventCloseVideoView -= vdoView_EventCloseVideoView;
                    vdoView.EventCloseVideoView += vdoView_EventCloseVideoView;
                });
            }
            logger.Info("Completed");
        }

        private bool ValidateVideoView(Document docItem)
        {
            logger.Info("Initiated");
            int count = 0;
            foreach (Control item in GridUserControlPanel.Children)
            {
                if (item is VideoView)
                {
                    docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                    {
                        DocumentDetails doc = item.DataContext as DocumentDetails;
                        if (doc != null)
                        {
                            if (doc.ID.Equals(c.ID))
                            {
                                item.BringIntoView();
                                Panel.SetZIndex(item, SetZIndexValue());
                                count++;
                            }
                        }
                    });
                }
            }
            if (count > 0)
            {
                logger.Info("Already Exists");
                return false;
            }
            else
            {
                logger.Info("Completed");
                return true;
            }
        }

        void vdoView_EventCloseVideoView(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            //remove VideoView from GridUserControlPanel
            VideoView vdoView = (VideoView)sender;
            GridPopupPanel.Children.Remove(vdoView);
            logger.Info("Completed");
        }
        #endregion

        void menu_EventOpenFeedback(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            //load MHCFeedbackUC
            MHCFeedbackUC feedback = new MHCFeedbackUC();
            feedback.BringIntoView();
            Panel.SetZIndex(feedback, SetZIndexValue());
            GridUserControlPanel.Children.Add(feedback);
            //raise events from MHCFeedbackUC
            feedback.EventCloseFeedbackMHC -= feedback_EventCloseFeedbackMHC;
            feedback.EventCloseFeedbackMHC += feedback_EventCloseFeedbackMHC;
            
            feedback.EventErrorAlert -= app_ErrorAlert;
            feedback.EventErrorAlert += app_ErrorAlert;

            feedback.EventWarningAlert -= app_WarningAlert;
            feedback.EventWarningAlert += app_WarningAlert;
            logger.Info("Completed");
        }
        
        void feedback_EventCloseFeedbackMHC(object sender, EventArgs e)
        {
            logger.Info("Initiated");
            RemoveGridUserControlPanel(Panels.MHCFeedbackUC);
            RemoveAllGridPopupPanel();
            logger.Info("Completed");
        }

        private void btnHomeLogo_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            LoadHomePage();
            logger.Info("Completed");
        }

        private void btnEmergency_TouchDown(object sender, TouchEventArgs e)
        {
            logger.Info("Initiated");
            EmergencyUC emergency = new EmergencyUC();
            emergency.BringIntoView();
            Panel.SetZIndex(emergency, SetZIndexValue());
            GridAdminSettingsPanel.Children.Add(emergency);

            emergency.EventCloseEmergencyUC -= emergency_EventCloseEmergencyUC;
            emergency.EventCloseEmergencyUC += emergency_EventCloseEmergencyUC;
            logger.Info("Completed");
        }

        void emergency_EventCloseEmergencyUC(object sender, EventArgs e)
        {
            logger.Info("Initiated");

            RemoveGridAdminSettingsPanel(Panels.EmergencyUC);
            logger.Info("Completed");
        }

        #region User Controls Remove
        private void RemoveGridUserControlPanel(Panels panel)
        {
            try
            {
                logger.Info("RemovePanel Initiated");
                List<int> lstIndexes = new List<int>();
                foreach (UIElement item in GridUserControlPanel.Children)
                {
                    switch (panel)
                    {
                        case Panels.SlideShowUC:
                            if (item is SlideShowUC)
                            {
                                ((SlideShowUC)item).Dispose();
                                lstIndexes.Add(GridUserControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.HomeMenuUC:
                            if (item is HomeMenuUC)
                            {
                                ((HomeMenuUC)item).Dispose();
                                lstIndexes.Add(GridUserControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.DepartmentUC:
                            if (item is DepartmentUC)
                            {
                                ((DepartmentUC)item).Dispose();
                                lstIndexes.Add(GridUserControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.TeamUC:
                            if (item is TeamUC)
                            {
                                ((TeamUC)item).Dispose();
                                lstIndexes.Add(GridUserControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.ContactUC:
                            if (item is ContactUC)
                            {
                                ((ContactUC)item).Dispose();
                                lstIndexes.Add(GridUserControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.CategoryUC:
                            if (item is CategoryUC)
                            {
                                ((CategoryUC)item).Dispose();
                                lstIndexes.Add(GridUserControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.DocumentsUC:
                            if (item is DocumentsUC)
                            {
                                ((DocumentsUC)item).Dispose();
                                lstIndexes.Add(GridUserControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.MHCFeedbackUC:
                            if (item is MHCFeedbackUC)
                            {
                                ((MHCFeedbackUC)item).Dispose();
                                lstIndexes.Add(GridUserControlPanel.Children.IndexOf(item));
                            }
                            break;
                        default:
                            break;
                    }
                }
                lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
                lstIndexes.ForEach(c =>
                {
                    GridUserControlPanel.Children.RemoveAt(c);
                });
                logger.Info("RemovePanel Completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }

        private void RemoveAllGridUserControlPanel()
        {
            RemoveGridUserControlPanel(Panels.SlideShowUC);
            RemoveGridUserControlPanel(Panels.HomeMenuUC);
            RemoveGridUserControlPanel(Panels.DepartmentUC);
            RemoveGridUserControlPanel(Panels.TeamUC);
            RemoveGridUserControlPanel(Panels.ContactUC);
            RemoveGridUserControlPanel(Panels.CategoryUC);
            RemoveGridUserControlPanel(Panels.DocumentsUC);
            RemoveGridUserControlPanel(Panels.MHCFeedbackUC);
        } 
        #endregion

        #region Popup Controls Remove
        private void RemoveGridPopupPanel(Panels panel)
        {
            try
            {
                logger.Info("RemovePanel Initiated");
                List<int> lstIndexes = new List<int>();
                foreach (UIElement item in GridPopupPanel.Children)
                {
                    switch (panel)
                    {
                        case Panels.ImageView:
                            if (item is ImageView)
                            {
                                ((ImageView)item).Dispose();
                                lstIndexes.Add(GridPopupPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.VideoView:
                            if (item is VideoView)
                            {
                                ((VideoView)item).Dispose();
                                lstIndexes.Add(GridPopupPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.BroucherUC:
                            if (item is BroucherUC)
                            {
                                ((BroucherUC)item).Dispose();
                                lstIndexes.Add(GridPopupPanel.Children.IndexOf(item));
                            }
                            break;
                        default:
                            break;
                    }
                }
                lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
                lstIndexes.ForEach(c =>
                {
                    GridPopupPanel.Children.RemoveAt(c);
                });
                logger.Info("RemovePanel Completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }

        private void RemoveAllGridPopupPanel()
        {
            RemoveGridPopupPanel(Panels.ImageView);
            RemoveGridPopupPanel(Panels.VideoView);
            RemoveGridPopupPanel(Panels.BroucherUC);
        } 
        #endregion

        #region Admin Remove Controls

        private void RemoveAllGridAdminSettingsPanel()
        {
            RemoveGridAdminSettingsPanel(Panels.AdminLogin);
            RemoveGridAdminSettingsPanel(Panels.AdminSettings);
            RemoveGridAdminSettingsPanel(Panels.SetAutoSyncTime);
            RemoveGridAdminSettingsPanel(Panels.EmergencyUC);
            RemoveGridAdminSettingsPanel(Panels.AdminLogin);
        }

        private void RemoveGridAdminSettingsPanel(Panels panel)
        {
            try
            {
                logger.Info("RemovePanel Initiated");
                List<int> lstIndexes = new List<int>();
                foreach (UIElement item in GridAdminSettingsPanel.Children)
                {
                    switch (panel)
                    {
                        case Panels.AdminLogin:
                            if (item is AdminLogin)
                            {
                                ((AdminLogin)item).Dispose();
                                lstIndexes.Add(GridAdminSettingsPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.AdminSettings:
                            if (item is AdminSettings)
                            {
                                ((AdminSettings)item).Dispose();
                                lstIndexes.Add(GridAdminSettingsPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.AutoSyncUC:
                            if (item is AutoSyncUC)
                            {
                                ((AutoSyncUC)item).Dispose();
                                lstIndexes.Add(GridAdminSettingsPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.SetAutoSyncTime:
                            if (item is SetAutoSyncTime)
                            {
                                ((SetAutoSyncTime)item).Dispose();
                                lstIndexes.Add(GridAdminSettingsPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.EmergencyUC:
                            if (item is EmergencyUC)
                            {
                                ((EmergencyUC)item).Dispose();
                                lstIndexes.Add(GridAdminSettingsPanel.Children.IndexOf(item));
                            }
                            break;
                        default:
                            break;
                    }
                }
                lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
                lstIndexes.ForEach(c =>
                {
                    GridAdminSettingsPanel.Children.RemoveAt(c);
                });
                logger.Info("RemovePanel Completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        } 
        #endregion

        #region Alert Remove Controls

        private void RemoveAllGridAlertControlPanel()
        {
            RemoveGridAlertControlPanel(Panels.WarningAlertUC);
            RemoveGridAlertControlPanel(Panels.SuccessAlertUC);
            RemoveGridAlertControlPanel(Panels.ErrorAlertUC);
            RemoveGridAlertControlPanel(Panels.UnderDevelopmentUC);
            RemoveGridAlertControlPanel(Panels.BrowserUc);
        }

        private void RemoveGridAlertControlPanel(Panels panel)
        {
            try
            {
                logger.Info("RemovePanel Initiated");
                List<int> lstIndexes = new List<int>();
                foreach (UIElement item in GridAlertControlPanel.Children)
                {
                    switch (panel)
                    {
                        case Panels.WarningAlertUC:
                            if (item is WarningAlertUC)
                            {
                                ((WarningAlertUC)item).Dispose();
                                lstIndexes.Add(GridAlertControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.SuccessAlertUC:
                            if (item is SuccessAlertUC)
                            {
                                ((SuccessAlertUC)item).Dispose();
                                lstIndexes.Add(GridAlertControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.ErrorAlertUC:
                            if (item is ErrorAlertUC)
                            {
                                ((ErrorAlertUC)item).Dispose();
                                lstIndexes.Add(GridAlertControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.UnderDevelopmentUC:
                            if (item is UnderDevelopmentUC)
                            {
                                ((UnderDevelopmentUC)item).Dispose();
                                lstIndexes.Add(GridAlertControlPanel.Children.IndexOf(item));
                            }
                            break;
                        case Panels.BrowserUc:
                            if (item is BrowserUc)
                            {
                                ((BrowserUc)item).Dispose();
                                lstIndexes.Add(GridAlertControlPanel.Children.IndexOf(item));
                            }
                            break;
                        default:
                            break;
                    }
                }
                lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
                lstIndexes.ForEach(c =>
                {
                    GridAlertControlPanel.Children.RemoveAt(c);
                });
                logger.Info("RemovePanel Completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }
        #endregion

        #region Sync Remove Controls

        private void RemoveAllGridSyncControlPanel()
        {
            RemoveGridSyncControlPanel(Panels.AutoSyncUC);
        }

        private void RemoveGridSyncControlPanel(Panels panel)
        {
            try
            {
                logger.Info("RemovePanel Initiated");
                List<int> lstIndexes = new List<int>();
                foreach (UIElement item in GridSyncControlPanel.Children)
                {
                    switch (panel)
                    {
                        case Panels.AutoSyncUC:
                            if (item is AutoSyncUC)
                            {
                                ((AutoSyncUC)item).Dispose();
                                lstIndexes.Add(GridSyncControlPanel.Children.IndexOf(item));
                            }
                            break;
                        default:
                            break;
                    }
                }
                lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
                lstIndexes.ForEach(c =>
                {
                    GridSyncControlPanel.Children.RemoveAt(c);
                });
                logger.Info("RemovePanel Completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
        }
        #endregion

    }
}
