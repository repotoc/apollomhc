﻿using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ApolloMHC.Converters
{
    public class PresentationIconVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Category)
            {
                Category selectedCateg = value as Category;
                if (selectedCateg == null)
                {
                    return Visibility.Hidden;
                }
                else
                {
                    if (selectedCateg.LstSubCategories == null || selectedCateg.LstSubCategories.Count == 0)
                    {
                        return CheckDocuments(selectedCateg.LstDocuments);
                    }
                    else
                    {
                        return selectedCateg.LstSubCategories.Where(c =>
                            c.LstDocuments.Any(d => d.DocumentType.Equals("PPT") || d.DocumentType.Equals("PDF"))).Count() > 0 ?
                            Visibility.Visible : Visibility.Hidden;
                    }
                }
            }
            return Visibility.Hidden;
        }

        private Visibility CheckDocuments(IList<Document> list)
        {
            if (list == null)
            {
                return Visibility.Hidden;
            }
            else if (list.Any(c => c.DocumentType.Equals("PPT") || c.DocumentType.Equals("PDF")))
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Hidden;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
