﻿using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
namespace ApolloMHC.Converters
{
    public class FeedbackSmileyVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string convParameter = System.Convert.ToString(parameter);
            List<string> lstQuestions = new List<string>();
            lstQuestions.Add("Have you used our services before?");
            lstQuestions.Add("Would you like to get information on subjects like health & lifestyle?");
            if (value is FeedBackQuestions)
            {
                FeedBackQuestions feedBackQuest = value as FeedBackQuestions;
                if (feedBackQuest == null)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    if (lstQuestions.Contains(feedBackQuest.QUESTION, StringComparer.InvariantCultureIgnoreCase))
                    {
                        return convParameter != "S" ? Visibility.Collapsed : Visibility.Visible;
                    }
                    else
                    {
                        return convParameter == "N" ? Visibility.Visible : Visibility.Collapsed;
                    }
                }
            }
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}