﻿using ApolloMHC.FeedbackQuestLogic;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ApolloMHC
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        public SplashScreen()
        {
            InitializeComponent();
        }
        #region variables
        BackgroundWorker bgw = null;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        #endregion

        private void SplashScreen_OnContentRendered(object sender, EventArgs e)
        {
            bgw = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true };
            bgw.DoWork += bgw_DoWork;
            bgw.ProgressChanged += bgw_ProgressChanged;
            bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
            bgw.RunWorkerAsync();
        }

        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblLoadingText.Text = e.UserState as string;
        }
        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                this.Hide();
                MainWindow HomePage = new MainWindow();
                HomePage.ShowDialog();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            Apps.myDbContext = new ApolloDB.DBFolder.ApolloDBContext();
            bgw.ReportProgress(1, "Loading Database...");
            Apps.myDbContext.FEEDBACKQUESTIONS.ToList();

            bgw.ReportProgress(2, "Intiating Feedback Questions...");
            FeedBackQuestionInitScript feedbackScript = new FeedBackQuestionInitScript();
            feedbackScript.RunInitScript();
        }
    }
}
