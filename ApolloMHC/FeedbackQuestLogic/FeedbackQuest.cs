﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApolloMHC.FeedbackQuestLogic
{
    public class FeedbackQuest
    {
        public string Name { get; set; }
    }
    public class FeedbackCategory
    {
        public FeedbackCategory()
        {
            LstFeedbackQuests = new List<FeedbackQuest>();
        }
        public string Name { get; set; }
        public List<FeedbackQuest> LstFeedbackQuests { get; set; }
    }

    public class PredefinedQuestions
    {
        public const string FirstQuest = "What made you choose Apollo Hospitals for your Personalised Health Check?";
        public const string SecondQuest = "Talking into account all health checks that you have experienced, how satisfied are you with the Personalised Health Check of Apollo Hospitals?";
        public const string ThirdQuest = "How likely are you to continue to come to Apollo Hospitals for your Personalised Health Check?";
        public const string FourthQuest = "How likely are you to recommend Apollo's Personalised Health Check to a friend or a family member?";
        public const string FifthQuest = "Ability of Personalised Health Check associates to effectively communicate and understand your needs?";
        public const string SixthQuest = "Value for money";
        public const string SeventhQuest = "Courtesy, Compassion &amp; Knowledge of the Appointment Associate.";
        public const string EigthQuest = "Convenience and ease of making an appointment for you Personalised Health Check.";
        public const string NinthQuest = "How do you reate us on providing necessary information and instructions before hand as preparation for the Health Check?";
        public const string TenthQuest = "The overall registration/billing process for your Personalised Health Check.";
        public const string EleventhQuest = "Courtesy and compassion of the registration/billing associate.";
        public const string TwelveQuest = "How well was the registration/billing associate able to customize the Pesonalised Health Check options to suite your health needs?";
        public const string ThirteenQuest = "Ease and comfort of finding your way to various counters for your Health Check.";
        public const string FourteenQuest = "The comfort and attractiveness of the waiting areas.";
        public const string FifteenQuest = "Upkeep of public washrooms.";
        public const string SixteenQuest = "Courtesy and Compassion exhibited by Personalised Health Check associates.";
        public const string SeventeenQuest = "Overall imaging services(includes X-rays, ultrasounds, sonograms, CT scans, mammography etc. that may have been performed).";
        public const string EighteenQuest = "Overall Laboratory services(includes blood, sample collections etc).";
        public const string NinteenQuest = "Overall Cardiology services(includes ECG, Echo, TMT etc.).";
        public const string TwentyQuest = "Other Investigations like (Dexascan, PFT etc.).";
        public const string TwentyOneQuest = "Concern for privacy and respect during tests.";
        public const string TwentyTwoQuest = "Waiting time for the services.";
        public const string TwentyThreeQuest = "Overall quality of food service in the restaurant";
        public const string TwentyFourQuest = "How would you like to rate us on your reports being made available as per time committed?";
        public const string TwentyFiveQuest = "How would you like to rate your experience on the wait time to see the doctor?";
        public const string TwentySixQuest = "Courtesy and compassion of the doctor.";
        public const string TwentySevenQuest = "How would you rate your experience on the quality of time spent by doctors to take you through your reports and suggest further health care interventions if required?";
    }

    public class PredefinedCategory
    {
        public const string Empty = " ";
        public const string OverAllExp = "OVERALL EXPERIENCE";
        public const string AppointmentExp = "APPOINTMENT EXPERIENCE";
        public const string RegBillExp = "REGISTRATION/BILLING EXPERIENCE";
        public const string LobbyExp = "LOBBY EXPERIENCE";
        public const string Diagnostic_AncillaryServices = "DIAGNOSTIC AND ANCILLARY SERVICES";
        public const string Food_BeverageExp = "FOOD & BEVERAGE EXPERIENCE";
        public const string ReportCollection = "REPORT COLLECTION";
        public const string ConsultationExp = "CONSULTATION EXPERIENCE";
    }
}
