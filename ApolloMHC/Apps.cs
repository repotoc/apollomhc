﻿using ApolloDB.DBFolder;
using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using NLog;
using ApolloMHC.UserControls;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace ApolloMHC
{
    public class Apps
    {
        public static ApolloDBContext myDbContext = new ApolloDBContext();

        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public static void InsertPageHit(string pageName)
        {
            TrackPageHits trackHits = new TrackPageHits
            {
                ID = myDbContext.TRACKPAGEHITS.ToList().Count > 0
                    ? myDbContext.TRACKPAGEHITS.Max(c => c.ID) + 1
                    : 1,
                PAGENAME = pageName,
                LASTUPDATEDTIME = DateTime.Now
            };

            myDbContext.TRACKPAGEHITS.Add(trackHits);
            myDbContext.SaveChanges();
        }
        public static void KillOSK()
        {
            //TabTip
            try
            {
                if (Process.GetProcessesByName("osk").Count() > 0)
                {
                    Process fetchProcess = Process.GetProcessesByName("osk").FirstOrDefault();
                    if (fetchProcess != null)
                    {
                        fetchProcess.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        public static void OpenVirtualKeyboard()
        {
            try
            {
                Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\osk.exe");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        public static void PingTest(bool raiseWindow = true)
        {
            try
            {
                Ping ping = new Ping();

                PingReply pingStatus = ping.Send("www.google.com");

                if (pingStatus.Status == IPStatus.Success)
                {

                }
                else
                {
                    if (raiseWindow)
                    {
                        InternetDown downDlg = new InternetDown();
                        downDlg.ShowDialog();
                    }
                }
            }
            catch (Exception)
            {
                if (raiseWindow)
                {
                    InternetDown downDlg = new InternetDown();
                    downDlg.ShowDialog();
                    downDlg.EventCloseWifi += downDlg_EventCloseWifi;
                }
            }
        }

        static void downDlg_EventCloseWifi(object sender, EventArgs e)
        {
            InternetDown downDlg = (InternetDown)sender;
            downDlg.Close();
        }

        public static bool PingCheck()
        {
            try
            {
                Ping ping = new Ping();
                PingReply pingStatus = ping.Send("www.google.com");
                if (pingStatus.Status == IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (PingException ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static bool ValidateAdmin(string userName,string password)
        {
            bool isValid = false;
            try
            {
                string serverPath = Utilities.SlideShowServerUrl;
                string cmcLoginPath = Utilities.CmcLoginService;
                string frameURI = serverPath + "/" + cmcLoginPath + "/";
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    Uri baseAddress = new Uri(frameURI);
                    client.BaseAddress = baseAddress;
                    if (!client.DefaultRequestHeaders.Contains("Authorization"))
                    {
                        byte[] authBytes = Encoding.UTF8.GetBytes((userName + ":" + password).ToCharArray());
                        client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(authBytes));
                    }
                    //string address = "api/Admin/ValidateAdmin?userName=" + userName + "&password=" + password;
                    string address = "api/Login?userName=" + userName + "&password=" + password + "&loginType=S";
                    HttpResponseMessage response = client.GetAsync(address).Result;
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        isValid = response.Content.ReadAsAsync<bool>().Result;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return isValid;

            //bool isValid = false;
            //try
            //{
            //    string serverPath = Utilities.SlideShowServerUrl;
            //    string hostedPath = Utilities.SlideShowHostedService;
            //    string frameURI = serverPath + "/" + hostedPath + "/";
            //    using (HttpClient client = new HttpClient())
            //    {
            //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //        Uri baseAddress = new Uri(frameURI);
            //        client.BaseAddress = baseAddress;
            //        if (!client.DefaultRequestHeaders.Contains("Authorization"))
            //        {
            //            byte[] authBytes = Encoding.UTF8.GetBytes((userName + ":" + password).ToCharArray());
            //            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(authBytes));
            //        }
            //        string address = "api/Admin/ValidateAdmin?userName=" + userName + "&password=" + password;
            //        HttpResponseMessage response = client.GetAsync(address).Result;
            //        if (response.StatusCode == HttpStatusCode.OK)
            //        {
            //            isValid = response.Content.ReadAsAsync<bool>().Result;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //Log Herer
            //}
            //return isValid;
        }
    }
}
